drop table if exists kook_message;
create table if not exists kook_message (
    `id` int(11) not null auto_increment comment '自增id',
    `channel_id` varchar(40) default null comment '频道id',
    `author_id` varchar(40) default null comment '消息发送者id',
    `receive_message_id` varchar(40) default null comment '收到的消息id',
    `receive_content` varchar(300) default null comment '收到的消息内容',
    `receive_timestamp` bigint(13) default null comment '收到的消息时间',
    `reply_message_id` varchar(40) default null comment '回复的消息id',
    `reply_content` varchar(300) default null comment '回复的消息内容',
    `reply_timestamp` bigint(13) default null comment '回复的消息时间',
    `bind_code` varchar(32) default null comment '绑定码',
    `account_id` int(11) default null comment '账号id',
    primary key(`id`) using btree,
    index(`bind_code`)
) engine = innodb auto_increment = 1 comment 'kook消息表';
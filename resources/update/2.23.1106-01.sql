drop table if exists account_extend;
create table if not exists account_extend(
    id int(11) not null auto_increment comment '自增id',
    account_id int(11) not null comment '账号id',
    extend_id varchar(50) default null comment '扩展id',
    extend_type varchar(50) default null comment '扩展类型',
    extend_value varchar(255) default null comment '扩展值',
    primary key(id) using btree,
    index(account_id, extend_id)
) engine = innodb auto_increment = 1 charset = gbk collate = gbk_chinese_ci comment '账号扩展表';
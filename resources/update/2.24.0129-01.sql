drop table if exists cash_item;
create table cash_item (
    `sn` int(11) not null,
    `item_id` int(11) not null,
    `count` int(11) null default 1,
    `price` int(11) null default 0,
    `priority` int(11) null default 0,
    `period` bigint(20) null default 1,
    `gender` int(1) null default 2,
    `on_sale` int(1) null default 0,
    primary key (`sn`)  using btree
) engine = innodb charset = gbk collate = gbk_chinese_ci comment '商城物品表';

drop table if exists cashshop_modified_items;
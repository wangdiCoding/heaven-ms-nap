drop table if exists character_extend;
create table if not exists character_extend(
    id int(11) not null auto_increment comment '自增id',
    character_id int(11) not null comment '角色id',
    extend_id varchar(50) default null comment '扩展id',
    extend_type varchar(50) default null comment '扩展类型',
    extend_value varchar(255) default null comment '扩展值',
    primary key(id) using btree,
    index(character_id, extend_id)
) engine = innodb auto_increment = 1 charset = gbk collate = gbk_chinese_ci comment '角色扩展表';
FROM openjdk:8u171-jdk-alpine
RUN echo "https://mirrors.aliyun.com/alpine/v3.14/main/" > /etc/apk/repositories && \
    echo "https://mirrors.aliyun.com/alpine/v3.14/community/" >> /etc/apk/repositories
RUN apk update && apk -U add tini
WORKDIR /mnt
COPY ./ ./
EXPOSE 8484 7575 7576 7577 7578 7579
ENTRYPOINT ["tini", "--"]
CMD ["java", "-Dwzpath=wz", "-jar", "dist/heaven-ms-nap.jar"]
CREATE TABLE `napms`.`gachapon`
(
    `id`             int(11) NOT NULL AUTO_INCREMENT,
    `gachapon_type`  varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '扭蛋类型',
    `npc_id`         int(30) NULL DEFAULT NULL COMMENT 'npc_id',
    `common_rate`    int(5) NULL DEFAULT NULL,
    `uncommon_rate`  int(5) NULL DEFAULT NULL,
    `rare_rate`      int(5) NULL DEFAULT NULL,
    `global_type`    varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '扭蛋物品类型',
    `global_level`   varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '扭蛋物品级别普通，特殊之类的意思',
    `global_item_id` int(15) NULL DEFAULT NULL COMMENT '扭蛋物品id',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=gbk COLLATE=gbk_chinese_ci ROW_FORMAT=Compact;


-- END
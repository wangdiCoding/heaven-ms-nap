# Docker搭建

## 1.Portainer（可选）

执行以下命令拉取portainer镜像

```bash
docker pull portainer/portainer
```

执行以下命令启动

```bash
docker run -d -p 9000:9000 --name portainer -v /var/run/docker.sock:/var/run/docker.sock --restart=always portainer/portainer
```

首次进入会让你设置admin的密码，一定要记住自己的密码。输完密码后，让你选管理的环境。依次是Local（本地）、Remote（远程）、Agent（代理）、Azure（微软云）。我们单机选择Local进入即可。

### 安装汉化（可选）

访问下载文件：[Release Portainer-CN2021-IMNKS.COM-20211018 · tbc0309/Portainer-CN2021 (github.com)](https://github.com/tbc0309/Portainer-CN2021/releases/tag/20211018)

解压后放到指定位置，如/home/public，然后执行

```bash
docker run -d -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data -v /home/public:/public portainer/portainer
```

## 2.MySQL

### 用Portainer

- Name-mysql57

- Image（镜像）-mysql:5.7

- Manual network port publishing（管理端口）-publish a new network port（映射端口）-3306:3306

- （可选）Volumes（卷）-Volume mapping（卷映射）-map additional volume-bind（绑定初始化sql路径）-/docker-entrypoint-initdb.d:/home/sql

- （可选）Volumes（卷）-Volume mapping（卷映射）-map additional volume-bind（绑定数据路径）-/var/lib/mysql:/home/data

- （可选）Network（网络）-Network（网络类型）-host

- Env（环境变量）-Environment variables（环境变量）-add environment variable（添加环境变量）-MYSQL_ROOT_PASSWORD:root

- Restart policy（重启策略）-Restart policy-Always

- Deploy the container（部署容器）

### Bash

执行以下命令拉取镜像

```bash
docker pull mysql:5.7
```

执行以下命令启动

```bash
docker run -it -d -p 3306:3306 -v /home/sql:/docker-entrypoint-initdb.d -e MYSQL_ROOT_PASSWORD=root --restart=always --name mysql57 mysql:5.7
```

## 3.服务端



### 用Portainer

Dockerfile如下

```dockerfile
FROM openjdk:8u171-jdk-alpine
RUN echo "https://mirrors.aliyun.com/alpine/v3.14/main/" > /etc/apk/repositories && \
    echo "https://mirrors.aliyun.com/alpine/v3.14/community/" >> /etc/apk/repositories
RUN apk update && apk -U add tini
WORKDIR /mnt
ADD https://gitee.com/sleepnap/heaven-ms-nap/releases/download/2.23.1222/2.23.1222.tar.gz 2.23.1222.tar.gz
RUN tar zxvf 2.23.1222.tar.gz -C /mnt/ --strip-components=1
RUN chmod +x dist/heaven-ms-nap.jar
EXPOSE 8484 7575 7576 7577 7578 7579
ENTRYPOINT ["tini", "--"]
CMD ["java", "-Dwzpath=wz", "-jar", "dist/heaven-ms-nap.jar"]
```



### Bash

将文件上传到服务器，然后解压

使用以下命令来查看mysql容器的ip

```bash
docker inspect xxx
```

根据mysql的容器ip，修改config_server下dbUrl的地址，将修改后的Server.db覆盖上传到服务器，替换掉原有的Server.db

执行以下命令构建docker镜像

```bash
docker build -t heavenms-nap:2.23.1222 .
```

Dockerfile如下

```dockerfile
FROM openjdk:8u171-jdk-alpine
RUN echo "https://mirrors.aliyun.com/alpine/v3.14/main/" > /etc/apk/repositories && \
    echo "https://mirrors.aliyun.com/alpine/v3.14/community/" >> /etc/apk/repositories
RUN apk update && apk -U add tini
WORKDIR /mnt
COPY ./ ./
EXPOSE 8484 7575 7576 7577 7578 7579
ENTRYPOINT ["tini", "--"]
CMD ["java", "-Dwzpath=wz", "-jar", "dist/heaven-ms-nap.jar"]
```

执行`docker images`可以看到构建出了一个heavenms-nap的镜像，Tag是2.23.1222

然后直接执行以下命令启动docker镜像就行

```bash
docker run -itd --restart=always --name heavenms-nap heavenms-nap:2.23.1222
```

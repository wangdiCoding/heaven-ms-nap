# 第四课
## ToggleButton和ToggleGroup
ToggleButton和普通的button差不多，但是他可以获取到选中状态，也可以设置到ToggleGroup  
ToggleGroup让多个ToggleButton互斥，只能同时有一个被选中  
有一个问题需要注意，ToggleButton可以被取消选中状态  

# 第九课
## ProgressIndicator
ProgressIndicator是JavaFx自带的进度条指示器  
-1就是一直未开始，1就是已完成  
## LinkedBlockingQueue和CompletableFuture
Java中Queue是队列，能够实现先进先出。Stack是栈，能够实现后进先出。  
LinkedBlockingQueue是Java常用一个队列，实现了Queue队列，支持队列的特性。但与其他队列不同，他是默认初始化的容量是Integer.MAX_VALUE，也叫无界队列，所以无需为扩容烦恼。  
CompletableFuture实现了Future接口，和FutureTask一样是常用的一个执行异步操作的类。但CompletableFuture提供了更多特性，还有很多好用的方法，如allOf、anyOf、thenApply、thenAccept等等，非常方便。  

# 第十课
## RadioButton
RadioButton和ToggleButton差不多，也是一个单选按钮，但是样式和ToggleButton不一样，更加像我们的单选框。  
## TilePane
TilePane和我们的FlowPane差不多，都是流式排列的，可以自适应。区别就是，他会把每一个子元素占用的大小给全部统一  
## Tooltip
鼠标移上去就会展示Tooltip设置的文本  

# 第十四课
## Pagination和TableView
Pagination是分页器，TableView是表格，这两个都是JavaFx自带的组件  

# 第二十课
## ContextMenu和MenuItem
ContextMenu是弹窗菜单，MenuItem是菜单项
var status = -1;

function start(mode, type, selection) {
    if (mode == 1) {
	status++;
    } else {
	if (status == 2) {
	    qm.sendNext("咋还磨蹭呢，赶紧地来锻炼了");
	    qm.dispose();
	    return;
	}
	status--;
    }
    if (status == 0) {
	qm.sendNext("好了，说明到这里就告一段落，咱们要进入下一阶段了。下一阶段是什么？刚才我已经说过了。就是不断的锻炼身体，直到你能干翻黑魔法师。");
    } else if (status == 1) {
	qm.sendNextPrev("虽然在几年前你确实挺牛的，但那也是过去的事情了。就算没有黑魔法师的诅咒，在冰块里封冻了那么久，蹦一下都费劲不是么？所以首先要做些热身运动。");
    } else if (status == 2) {
	qm.sendYesNo("都说内练一口气，外练筋骨皮！……这句话你也知道吧？当然要从#b 基本体力锻炼#k开始练起……啊，你先把气喘匀了，然后咱们开始基础体力锻炼。");
    } else if (status == 3) {
	qm.forceStartQuest();
	//qm.AranTutInstructionalBubble("Effect/OnUserEff.img/guideEffect/aranTutorial/tutorialArrow3");
	qm.dispose();
    }
}

function end(mode, type, selection) {
    qm.dispose();
}

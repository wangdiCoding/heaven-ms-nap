function enter(pi) {
  if (pi.isQuestActive(2314) || pi.getPlayer().getQuestStatus(2319) == 2) {
    if (pi.getQuestProgress(2314) != 1) {
      pi.setQuestProgress(2314, 1);
    }
    return true;
  }
  return false;
}

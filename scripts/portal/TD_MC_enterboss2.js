function enter(pi) {
    if(pi.isQuestCompleted(2331)) {
        pi.openNpc(1300013);
        return false;
    }
    
    if(pi.isQuestCompleted(2333) && pi.isQuestStarted(2331) && !pi.hasItem(4001318)){
        // 这里看起来是一个帮助用户完成任务的trick逻辑, 按照意义翻译一下
        pi.getPlayer().message("蘑菇王国的玉玺丢了？别担心，Kevin会送你一个备用的。");
        if(pi.canHold(4001318)){
            pi.gainItem(4001318, 1);
        }
        else{
            pi.getPlayer().message("伙计，你的背包满了，你打算怎么保存蘑菇王国的玉玺？");
        }
    }

    if(pi.isQuestCompleted(2333)){
        pi.playPortalSound();
        pi.warp(106021600, 1);
        return true;
    }
    else if(pi.isQuestStarted(2332) && pi.hasItem(4032388)){
        pi.forceCompleteQuest(2332, 1300002);
        pi.getPlayer().message("找到公主了！");
        pi.giveCharacterExp(4400, pi.getPlayer());
        
        var em = pi.getEventManager("MK_PrimeMinister");
        var party = pi.getPlayer().getParty();
        if (party != null) {
            var eli = em.getEligibleParty(pi.getParty());   // thanks Conrad for pointing out missing eligible party declaration here
            if(eli.size() > 0) {
                if (em.startInstance(party, pi.getMap(), 1)) {
                    pi.playPortalSound();
                    return true;
                } else {
                    pi.message("本频道已有其他队伍正在挑战Boss。");
                    return false;
                }
            }
        } else {
            if (em.startInstance(pi.getPlayer())) { // thanks RedHat for noticing an issue here
                pi.playPortalSound();
                return true;
            } else {
                pi.message("本频道已有其他队伍正在挑战Boss。");
                return false;
            }
        }
    }
    else if(pi.isQuestStarted(2333) || (pi.isQuestCompleted(2332) && !pi.isQuestStarted(2333))){
        var em = pi.getEventManager("MK_PrimeMinister");
        
        var party = pi.getPlayer().getParty();
        if (party != null) {
            var eli = em.getEligibleParty(pi.getParty());
            if(eli.size() > 0) {
                if (em.startInstance(party, pi.getMap(), 1)) {
                    pi.playPortalSound();
                    return true;
                } else {
                    pi.message("本频道已有其他队伍正在挑战Boss。");
                    return false;
                }
            }
        } else {
            if (em.startInstance(pi.getPlayer())) {
                pi.playPortalSound();
                return true;
            } else {
                pi.message("本频道已有其他队伍正在挑战Boss。");
                return false;
            }
        }
    }
    else{
        pi.getPlayer().message("门好像锁上了，也许我能找到钥匙打开它...");
        return false;
    }
}
function enter(pi) {
    if (pi.isQuestCompleted(2324)) {
        pi.playPortalSound(); pi.warp(106020501,0);
        return true;
    } else {
        pi.playerMessage(5, "前方的道路上布满了蔓延的藤蔓荆棘，只有用尖刺消除剂才能清除这些荆棘...");

        return false;
    }
}
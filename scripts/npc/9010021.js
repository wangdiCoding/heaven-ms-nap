/*
	This file is part of the OdinMS Maple Story Server
    Copyright (C) 2008 Patrick Huy <patrick.huy@frz.cc>
		       Matthias Butz <matze@odinms.de>
		       Jan Christian Meyer <vimes@odinms.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation version 3 as published by
    the Free Software Foundation. You may not use, modify or distribute
    this program under any other version of the GNU Affero General Public
    License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/* 9010021 - Wolf Spirit Ryko
    @author Ronan
 */
importPackage(Packages.client.command.commands.gm2)
 var status;

function start() {
    status = -1;
    if (!Packages.config.CommonConfig.config.server.useRebirthSystem) {
        cm.sendOk("GM未开启转生功能");
        cm.dispose();
        return;
    }
    action(1, 0, 0);
}

function action(mode, type, selection) {//捏麻麻滴，转生都来了是吧，跳过
    if (mode == 1) {
        status++;
    } else {
        cm.dispose();
        return;
    }
    if (status == 0) {
        cm.sendNext("您当前共转生 #r" + cm.getChar().getReborns() + " #k次");
    } else if (status == 1) {
        cm.sendSimple("你想让我做什么: \r\n \r\n #L0##b我想转生#l \r\n #L1##b下次再说#k#l");
    } else if (status == 2) {
        if (selection == 0) {
            if (cm.getChar().getLevel() == 200) {
                cm.sendYesNo("你确定要转生吗?");
            } else {
                cm.sendOk("你没有达到200级, 请提升等级到200之后再来找我.");
                cm.dispose();
            }
        } else if (selection == 1) {
            cm.sendOk("好的，再见")
            cm.dispose();
        }
    } else if (status == 3 && type == 1) {
        cm.getChar().executeReborn();
        new ResetSkillCommand().execute(cm.getClient(),[])
        cm.gainItem(1302000)
        cm.sendOk("你现在重生了. 已重生 #r" + cm.getChar().getReborns() + "#k 次");
        cm.dispose();
    }


}
var sText;
var typeIdx;
var mapIdx;

// 城镇
var towns = Array(
    Array("自由市场", 910000000, 0),
    Array("南港", 60000, 0),
    Array("彩虹村", 1000000, 0),
    Array("明珠港", 104000000, 0),
    Array("射手村", 100000000, 0),
    Array("魔法密林", 101000000, 0),
    Array("勇士部落", 102000000, 0),
    Array("废弃都市", 103000000, 0),
    Array("林中之城", 105040300, 0),
    Array("黄金海滩", 110000000, 0),
    Array("诺特勒斯号码头", 120000000, 0),
    Array("圣地", 130000000, 0),
    Array("里恩", 140000000, 0),
    Array("天空之城", 200000000, 0),
    Array("幸福村", 209000000, 0),
    Array("冰峰雪域", 211000000, 0),
    Array("玩具城", 220000000, 0),
    Array("水下世界", 230000000, 0),
    Array("神木村", 240000000, 0),
    Array("武陵", 250000000, 0),
    Array("百草堂", 251000000, 0),
    Array("地球防御本部", 221000000, 0),
    Array("童话村", 222000000, 0),
    Array("阿尔泰营地", 300000000, 0),
    Array("新叶城", 600000000, 0),
    Array("昭和村", 801000000, 0),
    Array("古代神社", 800000000, 0),
    Array("阿里安特", 260000000, 0),
    Array("玛加提亚", 261000000, 0),
    Array("新加坡", 540000000, 0),
    Array("驳船码头城", 541000000, 0),
    Array("马来西亚", 551000000, 0),
    Array("婚礼村", 680000000, 0),
    Array("时间神殿", 270000100, 0),
    Array("废都广场", 103040000, 0),
    Array("大海兽峡谷", 240070000, 0),
    Array("蘑菇城", 106020000, 0)
);

// 练级
var exps = Array(
    Array( "射手训练场------------1级-30级",104040000, 0),
    Array( "泥泞的河岸外围--------30级-50级",550000100, 0),
    Array( "露台大厅--------------40级-70级",220010500, 5000),
    Array( "初级修炼场------------50级-60级",250020000, 5000),
    Array( "巨人之林--------------60级-80级",105040306, 10000),
    Array( "乌鲁城入口------------70级-90级",541020000, 20000),
    Array( "天空之巢--------------90级-110级",240010700, 20000),
    Array( "火焰死亡战场----------100级-120级",240020100, 20000),
    Array( "火焰树林--------------110级-130级",240030101, 20000),
    Array( "乌鲁城中心------------120级-140级",541020500, 50000),
    Array( "被遗忘的龙之巢穴------130级-150级",240040511, 50000),
    Array( "忘却之路5-------------140级-160级",270030500, 50000),
    Array( "西海叉路",230010400, 50000),
    Array( "死亡之林Ⅳ",211041400, 50000),
    Array( "乌山入口",222010000, 50000),
    Array( "时间停止之间",220070301, 50000),
    Array( "消失的时间",220070201, 50000),
    Array( "时间通道",220050300, 50000),
    Array( "十年药草地",251010000, 50000),
    Array( "云彩公园Ⅲ",200040000, 50000),
    Array( "黑暗庭院Ⅰ",200010301, 50000),
    Array( "龙之巢穴入口",240040500, 50000),
    Array( "龙的峡谷",240040000, 50000),
    Array( "狼蛛洞穴",600020300, 50000),
    Array( "乌鲁庄园",541020000, 50000),
    Array( "大佛的邂逅",800020130, 50000)
);

// 野外BOSS
var wilds = Array(
    Array("马来西亚熊", 551000000, 100),
    Array("乌鲁城入口", 541020000, 3000),
    Array("火狸金融（黑道BOSS副本）", 801040000, 100),
    Array("闹钟入口", 220080000, 5000),
    Array("扎昆入口", 211042300, 1000),
    Array("暗黑龙王洞穴入口",240050400,1000)
);

// npc
var npcs = Array(
    Array("汉斯", 101000003, 0),
    Array("赫丽娜", 100000201, 0),
    Array("武术教练", 102000003, 0),
    Array("达克鲁", 103000003, 0),
    Array("凯琳", 120000101, 0),
    Array("三转教官", 211000001, 0),
    Array("四转教官", 240010501, 0),
    Array("萧公", 925020001, 0)
);

function start() {
    status = -1;
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (mode === -1) {
        cm.dispose();
        return;
    }
    if (status >= 0 && mode === 0) {
        cm.dispose();
        return;
    }
    if (mode === 1) {
        status++;
    } else {
        status--;
    }
    if (status === 0) {
        var text = "#e#kHeavenMS-Nap传送服务#k\r\n\r\n #L0##e#d城镇地图传送#l \r\n #L1#练级地图传送#l \r\n #L2#副本BOSS传送#l \r\n ";
        text += "#L3#NPC传送#l";
        cm.sendSimple(text);
    } else if (status === 1) {
        typeIdx = selection;
        var i = 0;
        if (selection === 0) {
            sText = "#b";
            for (i = 0; i < towns.length; i++) {
                sText += "#L" + i + "#" + towns[i][0] + "\r\n";
            }
            cm.sendSimple(sText);
        } else if (selection === 1) {
            sText = "#b";
            for (i = 0; i < exps.length; i++) {
                sText += "#L" + i + "#" + exps[i][0] + "\r\n";
            }
            cm.sendSimple(sText);
        } else if (selection === 2) {
            cm.openNpc(9900001, "boss");
        } else if (selection === 3) {
            sText = "#b";
            for (i = 0; i < npcs.length; i++) {
                sText += "#L" + i + "#" + npcs[i][0] + "\r\n";
            }
            cm.sendSimple(sText);
        } else {
            cm.dispose();
        }
    } else if (status === 2) {
        mapIdx = selection;
        var cost;
        var mapId;
        if (typeIdx === 0) {
            cost = towns[mapIdx][2];
            mapId = towns[mapIdx][1];
        } else if (typeIdx === 1) {
            cost = exps[mapIdx][2];
            mapId = exps[mapIdx][1];
        } else if(typeIdx === 2){
            cm.dispose();
        } else if (typeIdx === 3) {
            cost = npcs[mapIdx][2];
            mapId = npcs[mapIdx][1];
        }
        if (cm.getMeso() >= cost) {
            cm.gainMeso(-cost);
            cm.message("本次传送花费: " + cost + " 金币");
            cm.warp(mapId);
        }
    }
}


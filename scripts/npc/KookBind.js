var status;

function start() {
    status = -1;
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (mode === 1) {
        status++;
    } else {
        status--;
    }
    if (status === 0) {
        if (cm.isBindKook()) {
            cm.sendOk("您已经绑定过了，请不要重复绑定。");
            cm.dispose();
        } else {
            cm.sendGetText("首次绑定可以获得#r#e2.024#n#k万点卷，#r#e2024#n#k万金币\r\n\r\n请输入绑定码");
        }
    } else if (status === 1) {
        var inputText = cm.getText();
        if (isEmpty(inputText)) {
            cm.sendOk("绑定码不能为空！");
            cm.dispose();
            return;
        }
        if (inputText.length != 6) {
            cm.sendOk("绑定码长度不正确!");
            cm.dispose();
            return;
        }
        // 如果有解绑操作，需要记录是否领取过奖励，这样重复绑定也不会重复领取奖励
        var bindKookReward = cm.getPlayer().getAccountExtendValue("bindKookReward");
        if (cm.doBindKook(inputText)) {
            var text = "绑定成功";
            if (null == bindKookReward) {
                text += "，恭喜获得#r#e2.024#n#k万点卷，#r#e2024#n#k万金币";
                cm.getPlayer().saveOrUpdateAccountExtend("bindKookReward", null, 'true');
                cm.getPlayer().getCashShop().gainNx(20240);
                cm.getPlayer().gainMeso(20240000);
            }
            cm.sendOk(text);
            cm.dispose();
        } else {
            cm.sendOk("绑定失败，请检查绑定码是否正确！");
            cm.dispose();
        }
    } else {
        cm.dispose();
    }
}

function isEmpty(text) {
    if (text === null) {
        return true;
    }
    if (text.length === 0) {
        return true;
    }
    return text.replace(/(^\s*)|(\s*$)/g, "").length === 0;
}

/*
教学脚本-昨日小睡

type 上一条消息的类型
mode 0=结束对话 1=继续对话
selection 上一个步骤获取的选项，比如这个脚本
status 从-1开始，每进行一次对话加1，可以记录当前脚本进行到哪一步了
 */

// 是否捐赠选项
var isDonated;

function start() {
    status = -1;
    action(1, 0, 0);
}

function action(mode, type, selection) {
    // 最好这么写，而不是mode <= 0直接dispose，因为冒险岛有返回上一步的操作
    if (mode === 1) {
        status++;
    } else {
        status--;
    }
    if (0 === status) {
        conversation1(mode, selection);
    } else if (1 === status) {
        conversation2(mode, selection);
    } else if (2 === status) {
        conversation3(mode, selection);
    } else {
        cm.dispose();
    }
}

/* 第一层级对话 */
function conversation1(mode, selection) {
    // 不处理返回上一步的场景
    if (0 === mode) {
        cm.dispose();
        return;
    }
    var level = getLevel();
    var call = getCall(level);
    if (0 === level) {
        // 如果当前对话人的会员等级是0，则触发首次捐赠对话
        isDonated = true;
        cm.sendGetNumber("你好，" + call + "，我叫小睡。我一直有一个梦想，就是在冒险岛世界开一家属于自己的百货商店，但苦于一直筹不到启动资金。" +
            "我相信以为的经商之才，定能在冒险岛世界成就一番事业。所以，你愿意资助我吗？作为回报，在商店营收后，我每天会拿当日收益的10%作为分红。\r\n\r\n" +
            "输入捐赠点券的数量",
            0, 1, 999999999);
    } else {
        if (isOpenDate() && level > 0) {
            cm.sendOk("太感谢你了，我想启动资金已经足够了，我这就去准备开店相关的事情！");
            cm.dispose();
            return;
        }
        var text;
        if (isRewarded()) {
            text = "欢迎，" + call + "，请问有什么事情需要吩咐吗？\r\n\r\n#L0#继续捐赠#l";
        } else {
            text = "欢迎，" + call + "。最近生意挺好，我感觉我们的事业正在蒸蒸日上！\r\n\r\n#L0#继续捐赠#l\r\n#L1#领取分红#l"
        }
        cm.sendSimple(text);
    }
}

/* 第二层级对话 */
function conversation2(mode, selection) {
    // 不处理返回上一步的场景
    if (0 === mode) {
        cm.dispose();
        return;
    }
    // 获取旧等级
    var level = getLevel();
    if (isDonated) {
        // 检查捐赠点券是否超过人物所拥有的点券
        if (selection > cm.getPlayer().getCashShop().getCash(1)) {
            cm.sendOk("我知道你很想帮助我，但你身上的点券好像不太够...");
            cm.dispose();
            return;
        }
        // 如果是捐赠，此时selection传入的是输入的点券
        saveDonation(selection);
        if (0 === level) {
            var newLevel = getLevel();
            if (0 === level && 0 < newLevel) {
                cm.sendOk("太感谢你了，我想启动资金已经足够了，我这就去准备开店相关的事情！");
            } else {
                cm.sendOk("感谢你的支持！");
            }
            cm.dispose();
        }
    } else {
        // 如果不是捐赠，则selection就是选择项
        if (0 === selection) {
            // 选择0，继续捐赠
            if (5 === level) {
                cm.sendOk("感谢你对小睡的支持，我想我们事业已经足够庞大了！");
                cm.dispose();
            } else {
                isDonated = true;
                cm.sendGetNumber("看来你想要继续扩大我们的规模呀~", 0, 1, 999999999);
            }
        } else {
            // 选择0，领取分红
            saveReward(getReward(level))
        }
    }
}

/* 第三层级对话 */
function conversation3(mode, selection) {
    // 不处理返回上一步的场景
    if (0 === mode) {
        cm.dispose();
        return;
    }
    if (isDonated) {
        // 检查捐赠点券是否超过人物所拥有的点券
        if (selection > cm.getPlayer().getCashShop().getCash(1)) {
            cm.sendOk("我知道你很想帮助我，但你身上的点券好像不太够...");
            cm.dispose();
            return;
        }
        // 如果是捐赠，此时selection传入的是输入的点券
        saveDonation(selection);
        cm.sendOk("感谢你的支持！");
        cm.dispose();
    } else {
        cm.dispose();
    }
}

/* 保存捐赠 */
function saveDonation(donation) {
    var donationVal = parseInt(donation);
    if (0 === donationVal) {
        return;
    }
    // 扣减点券
    cm.getPlayer().getCashShop().gainCash(1, -1 * donationVal);

    var napShopDonated = cm.getPlayer().getExtendValue("napShopDonated");
    var newNapShopDonated;
    if (null == napShopDonated) {
        newNapShopDonated = donationVal;
    } else {
        newNapShopDonated = parseInt(napShopDonated) + donationVal
    }
    // 先保存捐赠的点券
    cm.getPlayer().saveOrUpdateExtend("napShopDonated", null, newNapShopDonated);
    // 获取小睡商店开店时间
    var napShopOpenDate = cm.getPlayer().getExtendValue("napShopOpenDate");
    if (null == napShopOpenDate) {
        // 保存开店时间
        var curr = new Date();
        var year = curr.getFullYear();
        var month = curr.getMonth() + 1;
        var day = curr.getDate();
        cm.getPlayer().saveOrUpdateExtend("napShopOpenDate", null, year + '-' + month + '-' + day);
    }
}

/* 保存奖励时间 */
function saveReward(reward) {
    cm.sendOk("当日收益：" + (parseInt(reward) * 10) + "，你的分红：" + reward + "。点券已发往你的账户，请注意查收~");
    cm.getPlayer().getCashShop().gainCash(1, reward);
    // 保存领取奖励时间
    var curr = new Date();
    var year = curr.getFullYear();
    var month = curr.getMonth() + 1;
    var day = curr.getDate();
    cm.getPlayer().saveOrUpdateExtend("napShopRewardDate", null, year + '-' + month + '-' + day);
    cm.dispose();
}

/* 获取称呼 */
function getCall(level) {
    if (0 === level) {
        return "陌生人";
    } else if (1 === level) {
        return cm.getPlayer().getName();
    } else if (2 === level) {
        return "朋友";
    } else if (3 === level) {
        return "合伙人";
    } else if (4 === level) {
        return "老大";
    } else {
        return "老板";
    }
}

/* 获取奖励 */
function getReward(level) {
    var min = 1024 * Math.floor(level / 2);
    var max = 1024 * Math.floor(level * 2);
    return Math.floor(Math.random() * (max - min) + min);
}

/* 获取捐赠等级 */
function getLevel() {
    // 获取小睡商店捐赠的点券
    var napShopDonated = cm.getPlayer().getExtendValue("napShopDonated");
    if (null == napShopDonated || napShopDonated < 1024) {
        return 0;
    } else if (napShopDonated < (1024 * 2)) {
        return 1;
    } else if (napShopDonated < (1024 * 3)) {
        return 2;
    } else if (napShopDonated < (1024 * 4)) {
        return 3;
    } else if (napShopDonated < (1024 * 5)) {
        return 4;
    } else {
        return 5;
    }
}

/* 是否是开业当天 */
function isOpenDate() {
    // 获取小睡商店开店时间
    var napShopOpenDate = cm.getPlayer().getExtendValue("napShopOpenDate");
    if (null == napShopOpenDate) {
        return true;
    }
    var curr = new Date();
    var open = new Date(napShopOpenDate);
    curr.setHours(0, 0, 0, 0);
    open.setHours(0, 0, 0, 0);
    return open >= curr;
}

/* 是否已领取奖励 */
function isRewarded() {
    // 获取上次获取奖励的时间
    var napShopRewardDate = cm.getPlayer().getExtendValue("napShopRewardDate");
    if (null == napShopRewardDate) {
        return false;
    }
    var curr = new Date();
    var reward = new Date(napShopRewardDate);
    curr.setHours(0, 0, 0, 0);
    reward.setHours(0, 0, 0, 0);
    return reward >= curr;
}
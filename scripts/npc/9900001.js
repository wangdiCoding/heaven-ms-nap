importPackage(Packages.constants.game)

function calcExperienceOnLevel(percentage) {
    // 使用示例
    // calcExperienceOnLevel(0.5)
    return ExpTable.getExpNeededForLevel(cm.getPlayer().getLevel()) * percentage
}

function start() {
    status = -1;
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (status >= 0 && mode === 0) {
        cm.dispose();
        return;
    }
    if (mode === 1) {
        status++;
    } else {
        status--;
    }
    if (status === 0) {
        var text = " \t\t\t  #e欢迎来到#rHeavenMS-Nap#b（感谢瑶瑶提供的拍卖行脚本）#k#n              \r\n";
        text += "\t\t你的点券: " + cm.getPlayer().getCashShop().getCash(1)
        text += " ，你的代金券: " + cm.getPlayer().getCashShop().getCash(4)
        text += " ，你的皇家点券: " + cm.getPlayer().getCashShop().getCash(2)
        text += " \r\n\r\n";
        text += "#L0#快捷传送#l \t #L1#快捷商店#l     \t #L2#兑换物品#l\r\n\r\n";
        text += "#L3#新人福利#l \t #L4#血量装备兑换#l  \t #L5#其余兑换#l\r\n\r\n";
        text += "#L6#经验查看#l \t #L7#每天副本经验兑换#b(感谢浩哥)#l \r\n\r\n";
        text += "#L8#转生#l    \t #L9#小睡的神秘商店#l \t #L10#绑定kook#l\r\n\r\n";
        text += "#L11#增加1000HP血量上限(50,000,000金币)#l \r\n\r\n";
        cm.sendSimple(text);
    } else if (selection === 0) {
        cm.openNpc(9900001, "goto");
    } else if (selection === 1) {
        cm.openNpc(9900001, "shop");
    } else if (selection === 2) {
        cm.openNpc(9900001, "BOSS兑换");
    } else if (selection === 3) {
        cm.openNpc(9900001, "新人兑换");
    } else if (selection === 4) {
        cm.openNpc(9900001, "血量装备兑换");
    } else if (selection === 5) {
        cm.openNpc(9900001, "其余兑换");
    } else if (selection === 6) {
        cm.sendOk("你当前等级能获得的总经验值为:" + calcExperienceOnLevel(1) + "\r\n" + "百分之三十就是:" + calcExperienceOnLevel(0.3));
    } else if (selection === 7) {
        cm.openNpc(9900001, "每天副本经验兑换");
    } else if (selection === 8) {
        cm.openNpc(9900001, "9010021");
    } else if (selection === 9) {
        cm.openNpc(9900001, "NapShop");
    } else if (selection === 10) {
        cm.openNpc(9900001, "KookBind");
    } else if (selection === 11) {
        cm.openNpc(9900001, "增加HP上限");
    } else {
        cm.sendOk("暂不开放，请等待功能完成");
        cm.dispose();
    }
}
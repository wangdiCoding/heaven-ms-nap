/*
    This file is part of the HeavenMS MapleStory Server
    Copyleft (L) 2016 - 2019 RonanLana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation version 3 as published by
    the Free Software Foundation. You may not use, modify or distribute
    this program under any other version of the GNU Affero General Public
    License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Dalair
	Medal NPC.

        NPC Equipment Merger:
        * @author Ronan Lana
 */

importPackage(Packages.client.processor.action);
importPackage(Packages.config);

var status;
var mergeFee = 50000;
var name;

function start() {
    status = -1;
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (mode == -1) {
        cm.dispose();
    } else {
        if (mode == 0 && type > 0) {
            cm.dispose();
            return;
        }
        if (mode == 1)
            status++;
        else
            status--;

        if(status == 0) {
            if (!Packages.config.CommonConfig.config.server.useEnableCustomNpcScript) {
                cm.sendOk("勋章排名系统目前不可用......");
                cm.dispose();
                return;
            }
            
            var levelLimit = !cm.getPlayer().isCygnus() ? 160 : 110;
            var selStr = "勋章排名系统目前不可用......因此，我正在提供#b装备融合#k服务！";//？？？这是在做什么，跳过了
            
            if (!Packages.config.CommonConfig.config.server.useStarterMerge && (cm.getPlayer().getLevel() < levelLimit || MakerProcessor.getMakerSkillLevel(cm.getPlayer()) < 3)) {
                selStr += "你必须拥有 #r制造等级 3#k，并且至少达到 #r等级 110#k (Cygnus Knight)、#r等级 160#k (other classes)，并且拥有 #r" + cm.numberWithCommas(mergeFee) + " 金币#k，才能使用这项服务。";
                cm.sendOk(selStr);
                cm.dispose();
            } else if (cm.getMeso() < mergeFee) {
                selStr += "很抱歉，这项服务需要花费 #r" + cm.numberWithCommas(mergeFee) + " 金币#k，但很遗憾你似乎没有足够的金钱。请稍后再来~";
                cm.sendOk(selStr);
                cm.dispose();
            } else {
                selStr += "花费 #r" + cm.numberWithCommas(mergeFee) + "#k 金币，将背包中不需要的装备融合进目前你身上装备，来提升装备的属性。提升基于你当前所装备物品的属性。";
                cm.sendNext(selStr);
            }
        } else if(status == 1) {
            selStr = "#r警告#b: 确认背包中有能够合并的物品。 #r其次#b: 物品能够参与融合。#k 物品一旦被选中，就会被彻底融合。\r\n\r\n在那之后，装备会变成 #r不可交易#k，并且已经参与融合的装备 #r无法被用来充当融合材料#k。\r\n\r\n";
            cm.sendGetText(selStr);
        } else if(status == 2) {
            name = cm.getText();
            
            if (cm.getPlayer().mergeAllItemsFromName(name)) {
                cm.gainMeso(-mergeFee);
                cm.sendOk("融合完成！感谢使用融合系统，快看看装备的新属性把。");
            } else {
                cm.sendOk("你的背包里没有装备: #b'" + name + "'#k");
            }
            
            cm.dispose();
        }
    }
}

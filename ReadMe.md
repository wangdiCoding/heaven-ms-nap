## 基于HeavenMS汉化优化，原项目地址:https://github.com/ronancpl/HeavenMS  
本项目仅供学习和参考使用，所有商业行为系使用者个人承担，与本人无关，。  

# HeavenMS-NapMS
## 服务端编译
自己编译，参考视频：https://www.bilibili.com/video/BV1yp4y1A7bE  
在右侧有一个简介、发行版、贡献者、动态，点发行版即可取已经编译好的版本，怎么运行也参考上面的视频  
注意，整个项目的编码格式换成了GBK，请不要用UTF-8进行编码!!!  
注意，整个项目的编码格式换成了GBK，请不要用UTF-8进行编码!!!  
注意，整个项目的编码格式换成了GBK，请不要用UTF-8进行编码!!!  

一些教程视频：https://www.bilibili.com/video/BV133411o7Nj  

## 客户端下载

- 083cn初始英文端：https://pan.baidu.com/s/1GAgyysoRqKsfv-ODvnGkfA 提取码：ysn1
- 083cn初始英文端：https://pan.baidu.com/s/17z3pBKu3jz5AOJmB9eFzqQ 提取码：f5zc
- 70%汉化小体积端: https://pan.baidu.com/s/1qhEgKyvz7HqWPdSfK8tkVA 提取码: ruct  ——一个热心网友提供的客户端

如果想联机也参考上面视频

## 进展 
支持中文：100% (已完成预期)  
服务端脚本汉化：70% (quest 文件夹已全部汉化，因大部分为网络获取到的汉化脚本，未进行细致核对，大部分仍是机器翻译，未对文本进行润色)  
完善拍卖行脚本：80%  
2.x 版本规划：80%  
控制台UI重构：80%  

bug看情况修复，大家有发现了反馈  

## 新旧版本控制台对比
### 旧版本  
![旧版本](docs/course/pics/o_merge.png)  
### 新版本
![新版本](docs/course/pics/n_merge.png)  
### 对比视频
https://www.bilibili.com/video/BV1bQ4y1L7FU/  

## 文档
[修改说明](docs/change/changelog.txt)  

[文档](docs/course/目录.md)  

## 发现的问题与解决办法
- 问题1. 选人物后弹回登录页面  
- 原因1：发现其他应用程序会占用冒险岛服务端8484端口，目前重启电脑第一个运行服务端再运行其他程序解决。  
- 原因2：配置文件中未配置服务器IP或未设置非本地服务器，设置后重启服务器解决。  
- 原因3：云服务器控制台未放行8484和频道端口。

## 部分目录与文件位置
- /scripts     脚本文件总目录  
- /scripts/npc NPC脚本目录  
- /scripts/npc/9900001.js 拍卖行脚本  
- /scripts/npc/commands.js @help命令脚本
- /wz/Etc.wz/Commodity.img.xml 商城物品清单
- /src/main/java/client/command/commandsExecutor.java @help 命令源码

## 参考
感谢B站UP [@asotI1O](https://space.bilibili.com/19606926) 提供的搭建教程：  
参考专栏：https://www.bilibili.com/read/cv13485508  

感谢B站UP [@言尽乐](https://space.bilibili.com/98904118) 提供的中文乱码修复教程：  
参考专栏：https://www.bilibili.com/read/cv20050598  

脚本编写教程:
https://www.bilibili.com/video/BV1eY4y1j75X  

# 错误码解决方法
## 083cn客户端频繁启动崩溃
可以尝试设置MapleStory.exe设置兼容模式以XP(SP3)方式启动

## 启动客户端时：error code:2147467259
支持在显示器分辨率60+的环境玩冒险岛
https://gitee.com/sleepnap/gms083-src/tree/master/gms083-starter/Release
```cpp
//下载4个插件：冒险岛.exe，MapleStoryEx.dll，fixbug_msexcr.dll，fixbug_screen_refresh_rate.dll
//1. 将原本游戏客户端启动程序修改为：MapleStory.exe
//2. 将冒险岛.exe和MapleStoryEx.dll放入同级目录下
//3. 在游戏客户端目录创建文件夹plugins\
//4. fixbug_msexcr.dll，fixbug_screen_refresh_rate.dll放入plugins目录中
```
![1](docs/client/1.png)  
![2](docs/client/2.png)  

## 无法选择角色进入游戏
可以正常登陆，可以正常创建角色，但是无法进入游戏。   
**原因是需要修该服务器配置， host 中填入真正的服务器IP。而非默认的127.0.0.1**

## 关于中文乱码
大部分系统来说一般不会出现乱码，但是安装系统的时候使用的非中文语言登陆客户端就会存在乱码的情况。   
**控制面板，时钟和区域，区域，管理，更改系统区域设置，当前系统区域选择中文（简体，中国），勾选 Beta版：使用Unicode UTF-8提供支持，重启电脑**

## 进入游戏打开背包卡死后闪退
1. 可能是背包中存在实际不存在的代码物品，这种情况删除数据库中的inventoryitems表中不存在的代码即可
2. 电脑中下载了官服，这种情况先去官服打开背包，将栏目选定在装备栏后退出游戏，再打开083版本即可,原因是可能共用了一个注册表。(感谢大仙和Datas)
package tools;

import cn.nap.constant.NapPunc;
import constants.net.ServerConstants;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

public class Log {
    private static final Set<ILog> logs = new HashSet<>();

    static {
        System.setProperty("log4j.configurationFile", ServerConstants.RESOURCE_DIR + "xml/log4j2.xml");
    }

    private static final Logger logger = LogManager.getLogger(Log.class);

    public static void addLogInterface(ILog logInterface) {
        logs.add(logInterface);
    }

    public static void removeLogInterface(ILog logInterface) {
        logs.remove(logInterface);
    }


    public static void debug() {
        debug(NapPunc.EMPTY);
    }

    public static void debug(Object msg) {
        if (Level.DEBUG.compareTo(logger.getLevel()) > -1) {
            return;
        }
        logger.debug(msg);
//        for (ILog iLog : logs) {
//            iLog.debug(msg);
//        }
    }

    public static void info() {
        info(NapPunc.EMPTY);
    }

    public static void info(Object msg) {
        if (Level.INFO.compareTo(logger.getLevel()) > -1) {
            return;
        }
        logger.info(msg);
//        for (ILog iLog : logs) {
//            iLog.info(msg);
//        }
    }

    public static void warn() {
        warn(NapPunc.EMPTY);
    }

    public static void warn(Object msg) {
        if (Level.WARN.compareTo(logger.getLevel()) > -1) {
            return;
        }
        logger.warn(msg);
//        for (ILog iLog : logs) {
//            iLog.warn(msg);
//        }
    }

    public static void error() {
        error(NapPunc.EMPTY);
    }

    public static void error(Object msg) {
        if (Level.ERROR.compareTo(logger.getLevel()) > -1) {
            return;
        }
        logger.error(msg);
//        for (ILog iLog : logs) {
//            iLog.error(msg);
//        }
    }

    public static void error(Throwable t) {
        if (Level.ERROR.compareTo(logger.getLevel()) > -1) {
            return;
        }
        String msg = "程序运行出现异常:";
        logger.error(msg, t);
//        for (ILog iLog : logs) {
//            iLog.error(t);
//        }
    }

    public static void error(Object msg, Throwable t) {
        if (Level.ERROR.compareTo(logger.getLevel()) > -1) {
            return;
        }
        logger.error(msg, t);
//        for (ILog iLog : logs) {
//            iLog.error(msg, t);
//        }
    }
}

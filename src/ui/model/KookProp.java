package ui.model;

public class KookProp {
    private String botToken;
    private String guildId;
    private String guildName;
    private String channelId;
    private String channelName;
    private String wsUrl;
    private String bindKeywords;

    public String getBotToken() {
        return botToken;
    }

    public void setBotToken(String botToken) {
        this.botToken = botToken;
    }

    public String getGuildId() {
        return guildId;
    }

    public void setGuildId(String guildId) {
        this.guildId = guildId;
    }

    public String getGuildName() {
        return guildName;
    }

    public void setGuildName(String guildName) {
        this.guildName = guildName;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getWsUrl() {
        return wsUrl;
    }

    public void setWsUrl(String wsUrl) {
        this.wsUrl = wsUrl;
    }

    public String getBindKeywords() {
        return bindKeywords;
    }

    public void setBindKeywords(String bindKeywords) {
        this.bindKeywords = bindKeywords;
    }
}

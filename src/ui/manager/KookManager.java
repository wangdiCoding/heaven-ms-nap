package ui.manager;

import cn.nap.utils.common.NapComUtils;
import cn.nap.utils.exception.NapEx;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import tools.Log;
import ui.model.KookProp;
import ui.service.KookReceiver;
import ui.service.KookSender;

import java.util.concurrent.atomic.AtomicBoolean;

public class KookManager {
    private static KookManager instance;
    private KookSender sender;
    private KookReceiver receiver;
    private KookProp kookProp = new KookProp();
    private final AtomicBoolean online = new AtomicBoolean(false);

    public static KookManager getInstance() {
        if (instance == null) {
            instance = new KookManager();
        }
        return instance;
    }

    public void online() {
        // 不指定就默认为我的服务器，测试用，如果需要测试，可以进入：https://kook.top/CEX2Rf
        KookProp kookProp = new KookProp();
        kookProp.setBotToken("1/MjY1ODM=/KApBmIWTjEEdcToreC+9gg==");
        kookProp.setGuildId("8337921265574822");
        kookProp.setChannelName("绑定频道");
        kookProp.setBindKeywords("绑定");
        online(kookProp);
    }

    public void online(String botToken, String guildName, String channelName, String bindKeywords) {
        KookProp kookProp = new KookProp();
        kookProp.setBotToken(botToken);
        kookProp.setGuildName(guildName);
        kookProp.setChannelName(channelName);
        kookProp.setBindKeywords(bindKeywords);
        online(kookProp);
    }

    public void online(KookProp kookProp) {
        try {
            this.kookProp = kookProp;
            Log.info("正在上线机器人");
            doInit();
            if (!online.get()) {
                throw new NapEx("上线失败");
            }
            Log.info("机器人上线成功");
        } catch (Exception e) {
            Log.error("机器人上线失败", e);
        }
    }

    public void offline() {
        try {
            Log.info("正在下线机器人");
            if (receiver != null) {
                receiver.close();
            }
            online.set(false);
            instance = null;
            Log.info("机器人下线成功");
        } catch (Exception e) {
            Log.error("机器人下线失败", e);
        }
    }

    public boolean isOnline() {
        return online.get();
    }

    private void doInit() {
        Log.info("正在初始化Kook");
        if (NapComUtils.isEmpty(kookProp.getBotToken())) {
            throw new NapEx("botToken不能为空");
        }
        if (NapComUtils.isEmpty(kookProp.getGuildId()) && NapComUtils.isEmpty(kookProp.getGuildName())) {
            throw new NapEx("服务器不能为空");
        }
        if (NapComUtils.isEmpty(kookProp.getChannelId()) && NapComUtils.isEmpty(kookProp.getChannelName())) {
            throw new NapEx("绑定频道不能为空");
        }
        sender = new KookSender(kookProp);
        Log.info("正在检查服务器");
        JSONObject guildListResult = sender.guildList();
        JSONArray guildListItems = guildListResult.getJSONArray("items");
        String actuallyGuildId = null;
        String actuallyGuildName = null;
        for (int i = 0; i < guildListItems.size(); i++) {
            JSONObject guild = guildListItems.getJSONObject(i);
            String guildId = guild.getString("id");
            String guildName = guild.getString("name");
            if (NapComUtils.isEmpty(kookProp.getGuildId()) && guildName.equals(kookProp.getGuildName())) {
                actuallyGuildId = guildId;
                actuallyGuildName = guildName;
                break;
            }
            if (NapComUtils.isEmpty(kookProp.getGuildName()) && guildId.equals(kookProp.getGuildId())) {
                actuallyGuildId = guildId;
                actuallyGuildName = guildName;
                break;
            }
        }
        if (NapComUtils.isEmpty(actuallyGuildId) && NapComUtils.isEmpty(actuallyGuildName)) {
            throw new NapEx("服务器不存在");
        }
        kookProp.setGuildId(actuallyGuildId);
        kookProp.setGuildName(actuallyGuildName);

        Log.info("正在检查绑定频道");
        JSONObject channelListResult = sender.channelList();
        JSONArray channelListItems = channelListResult.getJSONArray("items");
        String actuallyChannelId = null;
        String actuallyChannelName = null;
        for (int i = 0; i < channelListItems.size(); i++) {
            JSONObject channel = channelListItems.getJSONObject(i);
            String channelId = channel.getString("id");
            String channelName = channel.getString("name");
            if (NapComUtils.isEmpty(kookProp.getChannelId()) && channelName.equals(kookProp.getChannelName())) {
                actuallyChannelId = channelId;
                actuallyChannelName = channelName;
                break;
            }
            if (NapComUtils.isEmpty(kookProp.getChannelName()) && channelId.equals(kookProp.getChannelId())) {
                actuallyChannelId = channelId;
                actuallyChannelName = channelName;
                break;
            }
        }
        if (NapComUtils.isEmpty(actuallyChannelId) && NapComUtils.isEmpty(actuallyChannelName)) {
            Log.info("绑定频道不存在，正在创建");
            JSONObject channelCreateResult = sender.channelCreate();
            actuallyChannelId = channelCreateResult.getString("id");
            actuallyChannelName = channelCreateResult.getString("name");
            if (NapComUtils.isEmpty(actuallyChannelId) || NapComUtils.isEmpty(actuallyChannelName)) {
                throw new NapEx("绑定频道创建失败");
            }
        }
        kookProp.setChannelId(actuallyChannelId);
        kookProp.setChannelName(actuallyChannelName);

        Log.info("正在获取网关地址");
        JSONObject gatewayIndexResult = sender.gatewayIndex();
        String gatewayUrl = gatewayIndexResult.getString("url");
        if (NapComUtils.isEmpty(gatewayUrl)) {
            throw new NapEx("网关地址获取失败");
        }
        kookProp.setWsUrl(gatewayUrl);

        Log.info("正在连接Kook");
        receiver = new KookReceiver(kookProp, sender);
        receiver.connect();
        Log.info("初始化Kook完成");

        Log.info("正在检测在线状态");
        long start = System.currentTimeMillis();
        while ((System.currentTimeMillis() - start) < 10000) {
            if (receiver.isOpen()) {
                break;
            }
        }
        online.set(receiver.isOpen());
        Log.info("检测在线状态完成");
    }
}

package ui;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.server.Server;
import tools.Log;
import ui.constant.MenuEnum;
import ui.menu.MenuAbstract;
import ui.util.ImageUtil;
import ui.util.ViewStyle;

import java.util.HashMap;
import java.util.Map;

public class ServerApp extends Application {
    private double xOffset;
    private double yOffset;
    private final Map<String, MenuAbstract> menuMap = new HashMap<>();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setWidth(1280);
        primaryStage.setHeight(720);
        primaryStage.setTitle("HeavenMS-Nap 控制台");
        primaryStage.getIcons().add(ImageUtil.createImage("MapleIcon.png"));

        VBox root = new VBox();

        HBox stageHead = new HBox();
        stageHead.setMaxHeight(28);
        stageHead.setSpacing(0);
        stageHead.setPadding(new Insets(5, 10, 5, 10));

        ImageView iconView = new ImageView(ImageUtil.createImage("MapleIcon.png", 20));
        HBox titlePane = new HBox();
        titlePane.setAlignment(Pos.CENTER);
        Label titleLabel = new Label("HeavenMS-Nap 控制台");
        titleLabel.setFont(Font.font("System", FontWeight.BOLD, 16));
        titlePane.getChildren().add(titleLabel);
        Button minimizeButton = ImageUtil.createImageButton("minimize.png");
        minimizeButton.setOnAction(event -> primaryStage.setIconified(true));
        minimizeButton.setOnMouseEntered(event -> minimizeButton.setStyle("-fx-background-color: rgb(231, 234, 238)"));
        minimizeButton.setOnMouseExited(event -> minimizeButton.setStyle("-fx-background-color: transparent;"));

        Button maximizeButton = ImageUtil.createImageButton("maximize.png");
        maximizeButton.setOnAction(event -> primaryStage.setMaximized(!primaryStage.isMaximized()));
        maximizeButton.setOnMouseEntered(event -> maximizeButton.setStyle("-fx-background-color: rgb(231, 234, 238)"));
        maximizeButton.setOnMouseExited(event -> maximizeButton.setStyle("-fx-background-color: transparent;"));

        Button closeButton = ImageUtil.createImageButton("close.png");
        closeButton.setOnAction(event -> {
            primaryStage.close();
            Server.getInstance().shutdownInternal(false);
            System.exit(0);
        });
        closeButton.setOnMouseEntered(event -> closeButton.setStyle("-fx-background-color: red"));
        closeButton.setOnMouseExited(event -> closeButton.setStyle("-fx-background-color: transparent;"));
        HBox.setHgrow(titlePane, Priority.ALWAYS);

        stageHead.setOnMousePressed(event -> {
            xOffset = event.getScreenX() - primaryStage.getX();
            yOffset = event.getScreenY() - primaryStage.getY();
        });
        stageHead.setOnMouseClicked(event -> {
            if (event.getClickCount() < 2) {
                return;
            }
            primaryStage.setMaximized(!primaryStage.isMaximized());
        });
        stageHead.setOnMouseDragged(event -> {
            primaryStage.setX(event.getScreenX() - xOffset);
            primaryStage.setY(event.getScreenY() - yOffset);
        });
        stageHead.getChildren().addAll(iconView, titlePane, minimizeButton, maximizeButton, closeButton);

        HBox stageBody = new HBox();
        VBox menuVBox = new VBox();
        menuVBox.setSpacing(5);
        menuVBox.setPadding(new Insets(10));
        menuVBox.setAlignment(Pos.TOP_CENTER);
        ToggleGroup menuGroup = new ToggleGroup();
        for (MenuEnum menu : MenuEnum.values()) {
            ToggleButton menuButton = new ToggleButton(menu.getName());
            menuButton.setStyle("-fx-background-color: transparent;-fx-font-size: 14;-fx-font-weight: bold;-fx-pref-width: 150;-fx-pref-height: 30;-fx-min-width: 150;-fx-min-height: 30");
            ViewStyle menuButtonStyle = new ViewStyle(menuButton);
            menuButton.setOnMouseEntered(event -> {
                menuButtonStyle.updateAndSet("-fx-background-color: rgb(5, 155, 255);-fx-text-fill:white");
            });
            menuButton.setOnMouseExited(event -> {
                if (menuButton.isSelected()) {
                    menuButtonStyle.modifyAndSet("-fx-background-color: rgb(200, 200, 200)", "-fx-text-fill");
                } else {
                    menuButtonStyle.modifyAndSet("-fx-background-color: transparent", "-fx-text-fill");
                }
            });
            menuButton.selectedProperty().addListener((observable, oldValue, newValue) -> {
                try {
                    if (newValue) {
                        menuButtonStyle.modifyAndSet("-fx-background-color: rgb(200, 200, 200)", "-fx-text-fill");
                        MenuAbstract instance = menu.getClz().newInstance();
                        instance.init(primaryStage, stageBody);
                        menuMap.put(menu.getName(), instance);
                    } else {
                        menuButtonStyle.modifyAndSet("-fx-background-color: transparent", "-fx-text-fill");
                        MenuAbstract instance = menuMap.get(menu.getName());
                        if (instance != null) {
                            instance.destroy();
                        }
                    }
                } catch (Exception e) {
                    Log.error("未知异常：", e);
                }
            });
            menuButton.setToggleGroup(menuGroup);
            menuVBox.getChildren().add(menuButton);
        }
        stageBody.getChildren().addAll(menuVBox, new Separator(Orientation.VERTICAL));
        VBox.setVgrow(stageBody, Priority.ALWAYS);

        root.getChildren().addAll(stageHead, new Separator(Orientation.HORIZONTAL), stageBody);
        root.setStyle("-fx-background-radius: 10;-fx-border-radius: 10;-fx-background-color: white;-fx-border-color: rgb(172, 172, 172);-fx-border-width: 1;");
        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);

        //窗口最小化监听
        primaryStage.iconifiedProperty().addListener(e -> {
            //System.out.println("iconified: " + primaryStage.isIconified());
        });

        //窗口最大化监听
        primaryStage.maximizedProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                //System.out.println("maximized:" + t1);
                if(t1){
                    root.setStyle("-fx-background-radius: 0;-fx-border-radius: 10;-fx-background-color: white;-fx-border-color: rgb(172, 172, 172);-fx-border-width: 0;");
                }else{
                    root.setStyle("-fx-background-radius: 10;-fx-border-radius: 10;-fx-background-color: white;-fx-border-color: rgb(172, 172, 172);-fx-border-width: 1;");
                }
            }
        });

        primaryStage.setScene(scene);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.show();
    }
}

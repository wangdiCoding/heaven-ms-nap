package ui.util;

import cn.nap.utils.exception.NapEx;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ViewUtil {
    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new NapEx("�߳������쳣: " + e.getMessage());
        }
    }

    public static void centerOnParent(Stage primaryStage, Stage stage) {
        stage.setX(primaryStage.getX() + (primaryStage.getWidth() - stage.getWidth()) / 2);
        stage.setY(primaryStage.getY() + (primaryStage.getHeight() - stage.getHeight()) / 2);
    }

    public static void bottomRightOnParent(Stage primaryStage, Stage stage) {
        stage.setX(primaryStage.getX() + primaryStage.getWidth() - stage.getWidth() - 20);
        stage.setY(primaryStage.getY() + primaryStage.getHeight() - stage.getHeight() - 20);
    }

    public static void beforeShow(Stage primaryStage, Stage stage, Pane root) {
        root.setStyle("-fx-padding: 10;-fx-spacing: 10; -fx-background-color: white;-fx-border-color: rgb(172, 172, 172);-fx-border-width: 1;-fx-background-radius: 10;-fx-border-radius: 10");

        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);
        stage.setScene(scene);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.initOwner(primaryStage);
        stage.initModality(Modality.WINDOW_MODAL);
        centerOnParent(primaryStage, stage);
    }
}

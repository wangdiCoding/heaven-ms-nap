package ui.util;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;

public class ImageUtil {
    public static Image createImage(String path) {
        return createImage(path, 18);
    }

    public static Image createImage(String path, int size) {
        path = "resources/img/" + path;
        File file = new File(path);
        String url = file.toURI().toString();
        return new Image(url, size, size, true, true);
    }

    public static Button createImageButton(String path) {
        Button button = new Button();
        ImageView imageView = new ImageView(createImage(path));
        button.setGraphic(imageView);
        button.setStyle("-fx-background-color: transparent;");
        return button;
    }
}

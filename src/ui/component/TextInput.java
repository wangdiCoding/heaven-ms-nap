package ui.component;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class TextInput extends HBox {
    private final Label label;
    private final TextField textField;

    public TextInput(String text) {
        label = new Label(text);
        label.setPrefWidth(100);
        textField = new TextField();
        setAlignment(Pos.CENTER_LEFT);
        setSpacing(5);
        getChildren().addAll(label, textField);
    }

    public Label getLabel() {
        return label;
    }

    public TextField getTextField() {
        return textField;
    }
}

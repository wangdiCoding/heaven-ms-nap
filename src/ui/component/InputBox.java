package ui.component;

import cn.nap.utils.common.NapComUtils;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import ui.util.ButtonUtil;
import ui.util.ViewUtil;

import java.util.Arrays;
import java.util.List;

public class InputBox {

    public static String getInput(Stage primaryStage, String text) {
        return getInputs(primaryStage, text)[0];
    }

    public static String[] getInputs(Stage primaryStage, String... texts) {
        return getInputs(primaryStage, Arrays.asList(texts));
    }

    public static String[] getInputs(Stage primaryStage, List<String> texts) {
        return getInputs(primaryStage, texts, null);
    }

    public static String[] getInputs(Stage primaryStage, List<String> texts, List<String> defaults) {
        String[] inputs = new String[texts.size()];

        Stage stage = new Stage();
        VBox root = new VBox();
        root.setAlignment(Pos.CENTER);

        Label title = new Label("请输入");
        title.setStyle("-fx-font-size: 14; -fx-font-weight: bold");
        root.getChildren().add(title);
        for (int i = 0; i < texts.size(); i++) {
            String text = texts.get(i);
            TextInput textInput = new TextInput(text);
            if (NapComUtils.isNotEmpty(defaults)) {
                String value = defaults.get(i);
                textInput.getTextField().setText(value);
                inputs[i] = value;
            }
            root.getChildren().add(textInput);
            final int index = i;
            textInput.getTextField().textProperty().addListener((observable, oldValue, newValue) -> inputs[index] = newValue);
            if (texts.size() == 1) {
                textInput.getTextField().setOnAction(event -> stage.close());
            }
        }
        HBox hBox = new HBox();
        hBox.setStyle("-fx-spacing: 10;-fx-alignment: center_right");
        Button yesButton = ButtonUtil.createGreenButton("确定");
        Button noButton = ButtonUtil.createBlackButton("取消");
        hBox.getChildren().addAll(yesButton, noButton);
        root.getChildren().add(hBox);
        yesButton.setOnAction(event -> stage.close());
        noButton.setOnAction(event -> {
            Arrays.fill(inputs, null);
            stage.close();
        });
        root.setOnKeyReleased(event -> {
            if (KeyCode.ESCAPE == event.getCode()) {
                Arrays.fill(inputs, null);
                stage.close();
            }
        });

        stage.setWidth(300);
        stage.setHeight(80 + texts.size() * 33);
        ViewUtil.beforeShow(primaryStage, stage, root);
        stage.showAndWait();
        return inputs;
    }

    public static boolean isNo(String[] inputs) {
        boolean isNo = true;
        for (String input : inputs) {
            if (input != null) {
                isNo = false;
                break;
            }
        }
        return isNo;
    }
}

package ui.component;

import cn.nap.utils.common.NapComUtils;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class NumberTableCell {
    public static <T> Callback<TableColumn<T, Number>, TableCell<T, Number>> forTableColumn() {
        return list -> new TextFieldTableCell<>(new StringConverter<Number>() {
            @Override
            public String toString(Number number) {
                return number == null ? null : number.toString();
            }

            @Override
            public Number fromString(String string) {
                return new Number() {
                    @Override
                    public int intValue() {
                        return NapComUtils.isEmpty(string) ? 0 : Integer.parseInt(string);
                    }

                    @Override
                    public long longValue() {
                        return NapComUtils.isEmpty(string) ? 0L : Long.parseLong(string);
                    }

                    @Override
                    public float floatValue() {
                        return NapComUtils.isEmpty(string) ? 0F : Float.parseFloat(string);
                    }

                    @Override
                    public double doubleValue() {
                        return NapComUtils.isEmpty(string) ? 0D : Double.parseDouble(string);
                    }
                };
            }
        });
    }
}

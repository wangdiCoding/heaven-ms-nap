package ui.component;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;

import java.util.ArrayList;
import java.util.List;

public class TextChoose extends HBox {
    private final Label label;
    private final List<RadioButton> radioButtonList = new ArrayList<>();

    public TextChoose(String text, String... btnTexts) {
        label = new Label(text);
        getChildren().add(label);
        ToggleGroup toggleGroup = new ToggleGroup();
        for (String btnText : btnTexts) {
            RadioButton radioButton = new RadioButton(btnText);
            radioButton.setToggleGroup(toggleGroup);
            getChildren().add(radioButton);
            radioButtonList.add(radioButton);
        }
        setSpacing(5);
        setAlignment(Pos.CENTER);
    }

    public Label getLabel() {
        return label;
    }

    public RadioButton getChoose() {
        return radioButtonList.stream().filter(RadioButton::isSelected).findFirst().orElse(null);
    }

    public void select(int index) {
        radioButtonList.get(index).setSelected(true);
    }
}

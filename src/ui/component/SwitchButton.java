package ui.component;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public class SwitchButton extends StackPane {
    private final BooleanProperty on = new BooleanPropertyBase() {
        @Override
        public Object getBean() {
            return this;
        }

        @Override
        public String getName() {
            return "switched";
        }
    };
    private final HBox rectangleHBox;
    private final HBox circleHBox;

    public SwitchButton() {
        this(40, 20);
    }

    public SwitchButton(double width, double height) {
        rectangleHBox = new HBox();
        rectangleHBox.setAlignment(Pos.CENTER);
        Rectangle rectangle = new Rectangle(width, height);
        rectangle.setStrokeWidth(1);
        rectangle.setArcWidth(height);
        rectangle.setArcHeight(height);
        rectangleHBox.getChildren().add(rectangle);

        circleHBox = new HBox();
        circleHBox.setPadding(new Insets(0, 6, 0, 6));
        Circle circle = new Circle();
        circle.setRadius(height * 0.6 / 2);
        circleHBox.getChildren().add(circle);

        getChildren().addAll(rectangleHBox, circleHBox);
        initStyle();
        setOnMouseClicked(event -> {
            on.set(!on.get());
            initStyle();
        });
    }

    public boolean isOn() {
        return on.get();
    }

    public void setOnOff(boolean onOff) {
        on.set(onOff);
        initStyle();
    }

    public BooleanProperty onOffProperty() {
        return on;
    }

    private void initStyle() {
        Rectangle rectangle = (Rectangle) rectangleHBox.getChildren().get(0);
        Circle circle = (Circle) circleHBox.getChildren().get(0);
        if (on.get()) {
            rectangle.setStroke(Color.rgb(0, 103, 192));
            rectangle.setFill(Color.rgb(0, 103, 192));

            circle.setFill(Color.WHITE);
            circleHBox.setAlignment(Pos.CENTER_RIGHT);
        } else {
            rectangle.setStroke(Color.rgb(93, 93, 93));
            rectangle.setFill(Color.rgb(245, 245, 245));

            circle.setFill(Color.rgb(93, 93, 93));
            circleHBox.setAlignment(Pos.CENTER_LEFT);
        }
    }
}

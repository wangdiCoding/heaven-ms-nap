package ui.component;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class TextSwitch extends HBox {
    private final Label label;
    private final SwitchButton switchButton;

    public TextSwitch(String text) {
        this(text, 40, 20);
    }

    public TextSwitch(String text, double width, double height) {
        label = new Label(text);
        label.setPrefWidth(100);
        switchButton = new SwitchButton(width, height);
        setAlignment(Pos.CENTER_LEFT);
        setSpacing(5);
        getChildren().addAll(label, switchButton);
    }

    public Label getLabel() {
        return label;
    }

    public SwitchButton getSwitchButton() {
        return switchButton;
    }
}

package ui.component;

import com.sun.javafx.application.PlatformImpl;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ui.util.ImageUtil;
import ui.util.ViewUtil;

import java.util.concurrent.LinkedBlockingQueue;

public class MessageBox {
    public static final String INFO = "info";
    public static final String ERROR = "error";
    private static final LinkedBlockingQueue<Stage> queue = new LinkedBlockingQueue<>();

    public static void showInfo(Stage primaryStage, String text) {
        show(primaryStage, text, INFO);
    }

    public static void showError(Stage primaryStage, String text) {
        show(primaryStage, text, ERROR);
    }

    public static void show(Stage primaryStage, String text, String type) {
        Stage stage = new Stage();

        HBox root = new HBox();
        root.setStyle("-fx-spacing: 5;-fx-alignment: center_right;-fx-background-color: transparent");
        ImageView imageView = new ImageView(ImageUtil.createImage(INFO.equals(type) ? "info.png" : "error.png"));
        Label label = new Label(text);
        root.getChildren().addAll(imageView, label);

        primaryStage.xProperty().addListener((observable, oldValue, newValue) -> stage.setX(stage.getX() + newValue.doubleValue() - oldValue.doubleValue()));
        primaryStage.yProperty().addListener((observable, oldValue, newValue) -> stage.setY(stage.getY() + newValue.doubleValue() - oldValue.doubleValue()));

        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);
        stage.setScene(scene);
        stage.initStyle(StageStyle.TRANSPARENT);
        if (text.length() <= 14) {
            stage.setWidth(200);
            stage.setHeight(30);
        } else {
            stage.setWidth(300);
            stage.setHeight((int) Math.floor(text.length() / 20D) * 18 + 30);
        }
        stage.initOwner(primaryStage);
        stage.initModality(Modality.NONE);
        ViewUtil.bottomRightOnParent(primaryStage, stage);
        waitShow(stage);
    }

    private static void waitShow(Stage stage) {
        queue.offer(stage);
        new Thread(() -> {
            Stage nowStage;
            while ((nowStage = queue.peek()) != stage) {
                ViewUtil.sleep(100);
            }
            Platform.runLater(nowStage::show);
            ViewUtil.sleep(3000);
            PlatformImpl.runAndWait(nowStage::close);
            queue.poll();
        }).start();
    }
}

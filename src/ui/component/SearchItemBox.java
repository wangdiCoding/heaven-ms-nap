package ui.component;

import cn.nap.http.NapHttpClient;
import cn.nap.http.NapHttpProp;
import cn.nap.utils.common.NapComUtils;
import com.sun.javafx.application.PlatformImpl;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import server.MapleItemInformationProvider;
import tools.Log;
import tools.Pair;
import ui.util.ButtonUtil;
import ui.util.ViewUtil;

import java.io.InputStream;
import java.net.HttpURLConnection;

public class SearchItemBox extends Stage {

    public SearchItemBox(Stage primaryStage) {
        VBox root = new VBox();
        root.setAlignment(Pos.CENTER);

        HBox searchBox = new HBox();
        searchBox.setStyle("-fx-alignment: center;-fx-spacing: 5");
        TextField input = new TextField();
        input.setPromptText("输入物品id");
        Button searchButton = ButtonUtil.createBlackButton("查询");
        searchBox.getChildren().addAll(input, searchButton);

        ImageView imageView = new ImageView();
        Label name = new Label();
        name.setWrapText(true);
        Label desc = new Label();
        desc.setWrapText(true);
        Region spacer = new Region();

        HBox exitHBox = new HBox();
        exitHBox.setStyle("-fx-spacing: 10;-fx-alignment: center_right");
        Button noButton = ButtonUtil.createBlackButton("取消");
        exitHBox.getChildren().add(noButton);
        noButton.setOnAction(event -> close());
        root.setOnKeyReleased(event -> {
            if (KeyCode.ESCAPE == event.getCode()) {
                close();
            }
        });
        root.getChildren().addAll(searchBox, imageView, name, desc, spacer, exitHBox);
        VBox.setVgrow(spacer, Priority.ALWAYS);

        input.setOnAction(event -> onSearch(primaryStage, input, imageView, name, desc));
        searchButton.setOnAction(event -> onSearch(primaryStage, input, imageView, name, desc));

        setWidth(300);
        setHeight(200);
        ViewUtil.beforeShow(primaryStage, this, root);
    }

    private void onSearch(Stage primaryStage, TextField input, ImageView imageView, Label name, Label desc) {
        String text = input.getText();
        if (NapComUtils.isEmpty(text) || !NapComUtils.isNumber(text)) {
            MessageBox.showError(primaryStage, "请输入正确的物品id");
            return;
        }
        ProgressLoading loading = new ProgressLoading(primaryStage);
        loading.add("正在查询物品信息", () -> {
            Pair<String, String> nameDesc = MapleItemInformationProvider.getInstance().getNameDesc(Integer.parseInt(text));
            PlatformImpl.runAndWait(() -> {
                if (nameDesc == null) return;
                name.setText(nameDesc.getLeft());
                desc.setText(nameDesc.getRight());
            });
        });
        loading.add("正在获取图片", () -> {
            String url = "https://maplestory.io/api/GMS/83/item/" + text + "/icon";
            NapHttpProp prop = new NapHttpProp();
            prop.setCharset("UTF-8");
            prop.setConnectTimeout(5000);
            prop.setReadTimeout(5000);
            prop.setFullUrl(url);
            try {
                //InputStream inputStream = NapHttpClient.getInputStream(prop);
                HttpURLConnection client = NapHttpClient.getConn(prop,false,"GET");
                //https://blog.csdn.net/weixin_39991222/article/details/114762684
                int code = client.getResponseCode();
                if ( code == HttpURLConnection.HTTP_OK ||
                     code == HttpURLConnection.HTTP_CREATED ||
                     code == HttpURLConnection.HTTP_ACCEPTED) {
                    InputStream inputStream = client.getInputStream();
                    PlatformImpl.runAndWait(() -> imageView.setImage(new Image(inputStream)));
                } else {
                    //inputStream = client.getErrorStream();
                    PlatformImpl.runAndWait(() -> MessageBox.showError(primaryStage, "Status Code:" + String.valueOf(code)));
                }
            } catch (Exception e) {
                PlatformImpl.runAndWait(() -> MessageBox.showError(primaryStage, "获取图片失败"));
                Log.error(e);
            }
        });
        loading.execute();
    }
}

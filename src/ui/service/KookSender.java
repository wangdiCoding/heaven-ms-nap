package ui.service;

import cn.nap.constant.NapExConst;
import cn.nap.http.NapHttpClient;
import cn.nap.http.NapHttpProp;
import cn.nap.utils.common.NapComUtils;
import cn.nap.utils.exception.NapEx;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import tools.Log;
import ui.model.KookProp;
import ui.util.ViewUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Kook服务http发送器
 * 参考：<a href="https://developer.kookapp.cn/doc/reference">http</a>
 */
public class KookSender {
    private final String BASE_URL = "https://www.kookapp.cn/api/v3";
    private final KookProp kookProp;
    private final NapHttpProp httpProp = new NapHttpProp();
    private int rateLimitRemaining = 120;
    private long rateLimitReset = 0;

    public KookSender(KookProp kookProp) {
        this.kookProp = kookProp;
        httpProp.setConnectTimeout(6000);
        httpProp.setReadTimeout(6000);
        httpProp.setCharset("UTF-8");
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", "Bot " + kookProp.getBotToken());
        httpProp.setHeader(header);
    }

    public JSONObject guildList() {
        httpProp.setFullUrl(BASE_URL + "/guild/list");
        return doGet();
    }

    public JSONObject channelList() {
        httpProp.setFullUrl(BASE_URL + "/channel/list" + "?guild_id=" + kookProp.getGuildId());
        return doGet();
    }

    public JSONObject channelCreate() {
        httpProp.setFullUrl(BASE_URL + "/channel/create");
        JSONObject data = new JSONObject();
        data.put("guild_id", kookProp.getGuildId());
        data.put("name", kookProp.getChannelName());
        return doPost(data);
    }

    public JSONObject gatewayIndex() {
        httpProp.setFullUrl(BASE_URL + "/gateway/index?compress=0");
        return doGet();
    }

    public JSONObject directMessageCreate(String targetId, String content) {
        httpProp.setFullUrl(BASE_URL + "/direct-message/create");
        JSONObject data = new JSONObject();
        data.put("target_id", targetId);
        data.put("content", content);
        return doPost(data);
    }

    private JSONObject doGet() {
        try {
            // 限流
            rateLimitCheck();
            Log.debug("url：" + httpProp.getFullUrl() + "，header：" + JSON.toJSON(httpProp.getHeader()));
            Map<String, List<String>> resultHeader = new HashMap<>();
            String result = NapHttpClient.doGet(httpProp, resultHeader);
            Log.info("get：" + result + "，header：" + JSON.toJSON(resultHeader));
            return parseResult(result, resultHeader);
        } catch (Exception e) {
            throw new NapEx(e.getMessage());
        }
    }

    private JSONObject doPost(Object data) {
        return doPost(JSON.toJSONString(data));
    }

    private JSONObject doPost(String data) {
        try {
            // 限流
            rateLimitCheck();
            Log.debug("url：" + httpProp.getFullUrl() + "，header：" + JSON.toJSON(httpProp.getHeader()));
            Log.info("req：" + data);
            Map<String, List<String>> resultHeader = new HashMap<>();
            String result = NapHttpClient.doPost(httpProp, data, resultHeader);
            Log.info("rev：" + result + "，header：" + JSON.toJSON(resultHeader));
            return parseResult(result, resultHeader);
        } catch (Exception e) {
            throw new NapEx(e.getMessage());
        }
    }

    private void rateLimitCheck() {
        if (rateLimitRemaining <= 0) {
            Log.info("已达到Kook服务限流，等待 " + rateLimitReset + " s");
            ViewUtil.sleep(rateLimitReset * 1000);
        }
    }

    private JSONObject parseResult(String result, Map<String, List<String>> resultHeader) {
        List<String> remainingHeaderVal = resultHeader.get("X-Rate-Limit-Remaining");
        if (NapComUtils.isNotEmpty(remainingHeaderVal)) {
            rateLimitRemaining = Integer.parseInt(remainingHeaderVal.get(0));
        }
        List<String> resetHeaderVal = resultHeader.get("X-Rate-Limit-Reset");
        if (NapComUtils.isNotEmpty(resetHeaderVal)) {
            rateLimitReset = Long.parseLong(resetHeaderVal.get(0));
        }

        JSONObject resultObject = JSON.parseObject(result);
        if (NapComUtils.isEmpty(resultObject)) {
            throw new NapEx(NapExConst.NapExType.EMPTY.getCode(), "返回结果为空");
        }
        if (resultObject.getInteger("code") != 0) {
            throw new NapEx(String.valueOf(resultObject.getInteger("code")), resultObject.getString("message"));
        }
        JSONObject resultData = resultObject.getJSONObject("data");
        if (NapComUtils.isEmpty(resultData)) {
            throw new NapEx(NapExConst.NapExType.EMPTY.getCode(), "返回data为空");
        }
        return resultData;
    }
}

package ui.service;

import cn.nap.utils.common.NapComUtils;
import cn.nap.utils.secret.NapRanUtils;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import tools.Log;
import ui.dao.KookMessageDao;
import ui.model.KookMessage;
import ui.model.KookProp;

import java.net.URI;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Kook服务websocket接收器
 * 参考：<a href="https://developer.kookapp.cn/doc/websocket">websocket</a>
 */
public class KookReceiver extends WebSocketClient {
    private final KookProp kookProp;
    private final KookSender sender;
    private long lastPing;
    private Timer timer;
    private int lastSn = 0;
    private String lastSession;

    public KookReceiver(KookProp kookProp, KookSender sender) {
        super(URI.create(kookProp.getWsUrl()));
        this.kookProp = kookProp;
        this.sender = sender;
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        Log.info("正在与Kook服务建立连接");
        updateLastPing();
        if (timer != null) {
            return;
        }
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                // 30s执行一次
                if (System.currentTimeMillis() - lastPing <= 30000) {
                    return;
                }
                // 超过36s没有收到pong，重连
                if (System.currentTimeMillis() - lastPing > 36000 || isClosed()) {
                    Log.info("与Kook服务连接超时，正在重连");
                    doReconnect();
                    return;
                }
                // 超过30s没收到消息，发送一次ping，等于定时发送心跳
                JSONObject pingObject = new JSONObject();
                pingObject.put("s", 2);
                pingObject.put("sn", lastSn);
                send(pingObject.toJSONString());
            }
        }, 6000, 6000);
    }

    @Override
    public void onMessage(String s) {
        // 先更新ping时间，避免重连
        updateLastPing();
        // 检查hello信息
        if (NapComUtils.isEmpty(s)) {
            return;
        }
        JSONObject messageObject;
        // 不是json格式不处理
        try {
            messageObject = JSON.parseObject(s);
        } catch (Exception e) {
            Log.warn("未知的消息格式：" + s);
            return;
        }
        // 获取sn
        Integer sn = messageObject.getInteger("sn");
        if (sn != null) {
            lastSn = sn;
        }
        // 获取信令
        int signaling = messageObject.getIntValue("s");
        if (signaling < 0 || signaling > 6) {
            Log.warn("收到未知信令：" + s + "，消息：" + s);
            return;
        }
        // 只处理以下信令
        if (signaling == 0) {
            // 业务信令
            JSONObject data = messageObject.getJSONObject("d");
            if (NapComUtils.isEmpty(data)) {
                Log.warn("收到异常的业务信令：" + s);
                return;
            }
            // 判断是否为文字或者系统消息，其他消息不处理
            int type = data.getIntValue("type");
            if (type == 2 || type == 3 || type == 4 || type == 8) {
                Log.warn("收到非文字消息，无法解析，已跳过");
                return;
            }
            // 非频道消息跳过
            if (NapComUtils.equals(data.getString("target_id"), kookProp.getChannelId())) {
                resolveMessage(data);
            }
        } else if (signaling == 1) {
            // hello信令
            Log.info("与Kook服务连接成功");
            // 更新session
            JSONObject data = messageObject.getJSONObject("d");
            if (NapComUtils.isEmpty(data)) {
                Log.warn("收到异常的hello信令：" + s);
                return;
            }
            // 判断hello信令是否正确
            if (40103 == data.getIntValue("code")) {
                Log.warn("收到hello信令：token过期，请手动进行重连");
                close();
                return;
            } else if (0 != data.getIntValue("code")) {
                Log.warn("收到异常的hello信令：" + s);
                return;
            }
            // 更新session
            lastSession = data.getString("session_id");
        } else if (signaling == 3) {
            // 心跳pong信令
            updateLastPing();
            return;
        } else if (signaling == 5) {
            // reconnect信令
            Log.info("收到Kook服务端通知：当前连接已失效！正在关闭连接，请手动进行重连");
            close();
            return;
        }
        // 结束时再更新ping时间，避免重连
        updateLastPing();
    }

    @Override
    public void onClose(int i, String s, boolean b) {
        Log.info("正在关闭与Kook服务的连接");
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public void onError(Exception e) {
        Log.error("与Kook服务通信发送异常：", e);
    }

    /**
     * 加锁，避免更新后的ping时间被改小
     */
    private synchronized void updateLastPing() {
        lastPing = System.currentTimeMillis();
    }

    private void resolveMessage(JSONObject data) {
        Log.info("收到消息：" + data.toJSONString());
        String content = data.getString("content");
        if (content.length() > 200) {
            Log.warn("消息长度过长，已忽略");
            return;
        }
        if (!content.contains(kookProp.getBindKeywords())) {
            Log.warn("非绑定消息，已忽略");
            return;
        }
        String authorId = data.getString("author_id");
        String channelId = data.getString("target_id");
        String receiveMessageId = data.getString("msg_id");
        long receiveTimestamp = data.getLongValue("msg_timestamp", System.currentTimeMillis());
        // 查询是否已生成绑定码
        List<KookMessage> kookMessageList = KookMessageDao.selectBindCode(authorId, null);
        String bindCode;
        if (NapComUtils.isEmpty(kookMessageList)) {
            bindCode = NapRanUtils.generateStr(6);
            // 查询生成是否重复
            while (NapComUtils.isNotEmpty(KookMessageDao.selectBindCode(null, bindCode))) {
                bindCode = NapRanUtils.generateStr(6);
            }
        } else {
            bindCode = kookMessageList.get(0).getBindCode();
        }
        // 私聊绑定码
        String replyContent = "您的绑定码为：" + bindCode + "，请在游戏内输入绑定码完成绑定";
        JSONObject replyData = sender.directMessageCreate(authorId, replyContent);
        if (NapComUtils.isEmpty(kookMessageList)) {
            String replyMessageId = replyData.getString("msg_id");
            long replyTimestamp = replyData.getLongValue("msg_timestamp", System.currentTimeMillis());
            // 保存消息
            KookMessage kookMessage = new KookMessage();
            kookMessage.setChannelId(channelId);
            kookMessage.setAuthorId(authorId);
            kookMessage.setReceiveMessageId(receiveMessageId);
            kookMessage.setReceiveContent(content);
            kookMessage.setReceiveTimestamp(receiveTimestamp);
            kookMessage.setReplyMessageId(replyMessageId);
            kookMessage.setReplyContent(replyContent);
            kookMessage.setReplyTimestamp(replyTimestamp);
            kookMessage.setBindCode(bindCode);
            KookMessageDao.insert(kookMessage);
        }
    }

    private void doReconnect() {
        // 重设wsUrl
        String wsUrl = kookProp.getWsUrl();
        if (lastSn != 0 && NapComUtils.isNotEmpty(lastSession)) {
            wsUrl += "&resume=1&sn=" + lastSn + "&session_id=" + lastSession;
        }
        uri = URI.create(wsUrl);
        reconnect();
    }
}

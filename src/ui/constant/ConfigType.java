package ui.constant;

public enum ConfigType {
    WORLD("world", "��������"),
    DATABASE("database", "���ݿ�����"),
    GAME("game", "��Ϸ����"),
    SERVER("server", "��������"),
    DEBUG("debug", "��������"),
    UNKNOWN("unknown", "δ֪����");

    private final String type;
    private final String desc;

    ConfigType(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public static ConfigType ofType(String type) {
        for (ConfigType value : values()) {
            if (value.type.equals(type)) {
                return value;
            }
        }
        return UNKNOWN;
    }

    public String getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}

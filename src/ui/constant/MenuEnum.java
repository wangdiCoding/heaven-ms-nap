package ui.constant;

import ui.menu.MenuAbstract;
import ui.menu.MenuCommand;
import ui.menu.MenuConfig;
import ui.menu.MenuDefault;
import ui.menu.MenuKook;
import ui.menu.MenuMall;
import ui.menu.MenuOperate;
import ui.menu.MenuPlayer;
import ui.menu.MenuPopular;

public enum MenuEnum {
    OPERATE("服务启停", MenuOperate.class),
    CONFIG("服务配置", MenuConfig.class),
    POPULAR("常用功能", MenuPopular.class),
    PLAYER("玩家管理", MenuPlayer.class),
    COMMAND("指令管理", MenuCommand.class),
    KOOK("KOOK管理", MenuKook.class),
    MERCHANT("商城管理", MenuMall.class),
    WZ("WZ配置", MenuDefault.class),
    ;

    private final String name;
    private final Class<? extends MenuAbstract> clz;


    MenuEnum(String name, Class<? extends MenuAbstract> clz) {
        this.name = name;
        this.clz= clz;
    }

    public String getName() {
        return name;
    }

    public Class<? extends MenuAbstract> getClz() {
        return clz;
    }
}

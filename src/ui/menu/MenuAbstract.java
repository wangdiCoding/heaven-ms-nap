package ui.menu;

import javafx.geometry.Insets;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public abstract class MenuAbstract {
    private Region root;
    protected Stage primaryStage;
    private HBox parent;

    public void init(Stage primaryStage, HBox parent) {
        this.primaryStage = primaryStage;
        this.parent = parent;
        show();
    }

    public void destroy() {
        if (root == null) {
            return;
        }
        parent.getChildren().remove(root);
        root = null;
    }

    protected abstract void show();

    protected void setRoot(Region root) {
        root.setPadding(new Insets(10));
        this.root = root;
        parent.getChildren().add(root);
        HBox.setHgrow(root, Priority.ALWAYS);
    }
}

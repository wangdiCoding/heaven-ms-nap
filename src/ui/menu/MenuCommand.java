package ui.menu;

import client.command.CommandsExecutor;
import cn.nap.utils.common.NapComUtils;
import com.sun.javafx.application.PlatformImpl;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Pagination;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.converter.IntegerStringConverter;
import net.server.Server;
import tools.Log;
import ui.component.ConfirmBox;
import ui.component.MessageBox;
import ui.component.ProgressLoading;
import ui.component.SwitchButton;
import ui.dao.GmCommandDao;
import ui.model.GmCommand;
import ui.util.ButtonUtil;

import java.util.ArrayList;
import java.util.List;

public class MenuCommand extends MenuAbstract {
    @Override
    protected void show() {
        VBox root = new VBox();
        root.setSpacing(5);

        HBox top = new HBox();
        top.setAlignment(Pos.CENTER_LEFT);
        top.setSpacing(5);
        TextField searchInput = new TextField();
        searchInput.setPromptText("请输入指令码/描述");
        ComboBox<Integer> levelComoBox = new ComboBox<>(FXCollections.observableArrayList(0, 1, 2, 3, 4, 5, 6));
        levelComoBox.setPromptText("请选择指令等级");
        levelComoBox.setStyle("-fx-background-color: transparent;-fx-border-color: gray;-fx-border-width: 1;-fx-border-radius: 5");
        Button searchButton = ButtonUtil.createBlackButton("查询");
        Button resetSearchButton = ButtonUtil.createBlackButton("重置");
        Button resetCommandButton = ButtonUtil.createBlackButton("恢复默认");
        Button saveButton = ButtonUtil.createGreenButton("保存");
        top.getChildren().addAll(searchInput, levelComoBox, searchButton, resetSearchButton, resetCommandButton, saveButton);

        List<GmCommand> originalCommands = new ArrayList<>();
        List<GmCommand> changeCommands = new ArrayList<>();
        Pagination center = new Pagination(1);
        center.setPageFactory(index -> createTableView(originalCommands, changeCommands));

        searchInput.setOnAction(event -> onSearch(center, searchInput, levelComoBox, originalCommands, changeCommands));
        searchButton.setOnAction(event -> onSearch(center, searchInput, levelComoBox, originalCommands, changeCommands));
        resetSearchButton.setOnAction(event -> {
            searchInput.setText(null);
            levelComoBox.getSelectionModel().select(-1);
            originalCommands.clear();
            changeCommands.clear();
            center.setPageFactory(null);
            center.setPageCount(1);
        });
        resetCommandButton.setOnAction(event -> {
            if (!ConfirmBox.both(primaryStage, "确认要把所有的指令恢复成默认吗？")) {
                return;
            }
            GmCommandDao.resetAll();
            onSearch(center, searchInput, levelComoBox, originalCommands, changeCommands);
            if (!Server.getInstance().isOnline()) {
                return;
            }
            CommandsExecutor.getInstance().resetAll();
        });
        saveButton.setOnAction(event -> {
            if (!ConfirmBox.both(primaryStage, "确认进行保存吗？")) {
                return;
            }
            if (NapComUtils.isEmpty(changeCommands)) {
                return;
            }
            try {
                GmCommandDao.updateList(changeCommands);
                MessageBox.showInfo(primaryStage, "保存成功！");
            } catch (Exception e) {
                MessageBox.showError(primaryStage, "保存失败！");
                Log.error("保存常用配置异常", e);
                return;
            }
            originalCommands.forEach(oriGmCommand -> changeCommands.stream()
                    .filter(change -> NapComUtils.equals(change.getId(), oriGmCommand.getId()))
                    .findFirst()
                    .ifPresent(change -> {
                        oriGmCommand.setGmLevel(change.getGmLevel());
                        oriGmCommand.setIsWork(String.valueOf(change.getIsWork()));
                    }));
            if (!Server.getInstance().isOnline()) {
                return;
            }
            ProgressLoading loading = new ProgressLoading(primaryStage);
            loading.add("正在重载指令...", () -> {
                try {
                    CommandsExecutor.getInstance().reloadGmCommands(changeCommands);
                } catch (Exception e) {
                    PlatformImpl.runAndWait(() -> ConfirmBox.yes(primaryStage, "重载指令失败，请检查指令是否正确！"));
                    Log.error("重载指令失败：", e);
                } finally {
                    changeCommands.clear();
                }
            });
            loading.execute();
        });

        root.getChildren().addAll(top, new Separator(Orientation.HORIZONTAL), center);
        VBox.setVgrow(center, Priority.ALWAYS);
        setRoot(root);
    }

    private void onSearch(Pagination center, TextField searchInput, ComboBox<Integer> levelComoBox, List<GmCommand> originalCommands, List<GmCommand> changeCommands) {
        String searchText = searchInput.getText();
        int selectedIndex = levelComoBox.getSelectionModel().getSelectedIndex();
        ProgressLoading loading = new ProgressLoading(primaryStage);
        final int PAGE_SIZE = 20;
        loading.add("正在查询总页数", () -> {
            int count = GmCommandDao.selectCountByCondition(searchText, selectedIndex);
            PlatformImpl.runAndWait(() -> center.setPageCount((int) Math.ceil((double) count / PAGE_SIZE)));
        });
        loading.execute();
        center.setPageFactory(index -> {
            TableView<GmCommand> tableView = createTableView(originalCommands, changeCommands);

            ProgressLoading searchLoading = new ProgressLoading(primaryStage);
            searchLoading.add("正在分页查询配置", () -> {
                // 每次翻页都清空changeList
                changeCommands.clear();
                ObservableList<GmCommand> commands = FXCollections.observableArrayList();
                originalCommands.clear();
                originalCommands.addAll(GmCommandDao.selectPageByCondition(index, PAGE_SIZE, searchText, selectedIndex));
                originalCommands.forEach(command -> commands.add(command.clone()));
                PlatformImpl.runAndWait(() -> tableView.setItems(commands));
            });
            searchLoading.execute();
            return tableView;
        });
    }

    private TableView<GmCommand> createTableView(List<GmCommand> originalCommands, List<GmCommand> changeCommands) {
        TableView<GmCommand> tableView = new TableView<>();
        tableView.setEditable(true);
        tableView.setStyle("-fx-font-size: 14");

        TableColumn<GmCommand, String> codeColumn = new TableColumn<>("指令码");
        tableView.getColumns().add(codeColumn);
        codeColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.2));
        codeColumn.setCellValueFactory(new PropertyValueFactory<>("command"));

        TableColumn<GmCommand, String> descColumn = new TableColumn<>("指令描述");
        tableView.getColumns().add(descColumn);
        descColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.4));
        descColumn.setCellValueFactory(new PropertyValueFactory<>("description"));

        TableColumn<GmCommand, Number> defaultColumn = new TableColumn<>("默认等级");
        tableView.getColumns().add(defaultColumn);
        defaultColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        defaultColumn.setCellValueFactory(new PropertyValueFactory<>("gmLevelPrimitive"));

        TableColumn<GmCommand, Integer> levelColumn = new TableColumn<>("当前等级");
        tableView.getColumns().add(levelColumn);
        levelColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        levelColumn.setCellValueFactory(new PropertyValueFactory<>("gmLevel"));
        levelColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        levelColumn.setOnEditCommit(event -> {
            Integer newValue = event.getNewValue();
            int row = event.getTablePosition().getRow();
            GmCommand editCommand = tableView.getItems().get(row);
            GmCommand oriCommand = originalCommands.get(row);
            editCommand.setGmLevel(newValue);
            if (notChange(editCommand, oriCommand)) {
                changeCommands.removeIf(change -> NapComUtils.equals(editCommand.getId(), oriCommand.getId()));
                return;
            }
            if (!changeCommands.contains(editCommand)) {
                changeCommands.add(editCommand);
            }
        });

        TableColumn<GmCommand, HBox> statusColumn = new TableColumn<>("是否启用");
        tableView.getColumns().add(statusColumn);
        statusColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.2));
        statusColumn.setCellValueFactory(param -> {
            HBox hBox = new HBox();
            hBox.setAlignment(Pos.CENTER);
            SwitchButton switchButton = new SwitchButton();
            switchButton.setOnOff(param.getValue().getIsWork());
            switchButton.onOffProperty().addListener((observable, oldValue, newValue) -> {
                int row = tableView.getSelectionModel().getSelectedIndex();
                GmCommand editCommand = tableView.getItems().get(row);
                GmCommand oriCommand = originalCommands.get(row);
                editCommand.setIsWork(newValue ? "true" : "false");
                if (notChange(editCommand, oriCommand)) {
                    changeCommands.removeIf(change -> NapComUtils.equals(editCommand.getId(), oriCommand.getId()));
                    return;
                }
                if (!changeCommands.contains(editCommand)) {
                    changeCommands.add(editCommand);
                }
            });
            hBox.getChildren().add(switchButton);
            return new SimpleObjectProperty<>(hBox);
        });

        return tableView;
    }

    private boolean notChange(GmCommand editCommand, GmCommand oriCommand) {
        return NapComUtils.equals(editCommand.getGmLevel(), oriCommand.getGmLevel()) && NapComUtils.equals(editCommand.getIsWork(), oriCommand.getIsWork());
    }
}

package ui.menu;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class MenuDefault extends MenuAbstract{
    @Override
    public void show() {
        VBox root = new VBox();
        root.setAlignment(Pos.CENTER);
        Label label = new Label("该功能不在2.x版本支持，请关注后续版本！");
        label.setStyle("-fx-font-size: 26px;-fx-font-weight: bold;-fx-text-fill: linear-gradient(from 0% 0% to 100% 100%, blueviolet, deeppink)");
        root.getChildren().add(label);
        setRoot(root);
    }


}

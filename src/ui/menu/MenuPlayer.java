package ui.menu;

import client.MapleCharacter;
import client.MapleJob;
import client.inventory.Equip;
import client.inventory.MapleInventoryType;
import cn.nap.utils.common.NapComUtils;
import constants.inventory.ItemConstants;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import net.server.Server;
import net.server.channel.Channel;
import server.MapleItemInformationProvider;
import ui.component.ConfirmBox;
import ui.component.InputBox;
import ui.component.MessageBox;
import ui.util.ButtonUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

public class MenuPlayer extends MenuAbstract {
    private Timer timer;

    @Override
    protected void show() {
        VBox root = new VBox();
        root.setSpacing(5);

        HBox top = new HBox();
        top.setAlignment(Pos.CENTER_LEFT);
        top.setSpacing(5);
        TextField searchInput = new TextField();
        searchInput.setPromptText("请输入账号ID/玩家名称");
        Button refreshButton = ButtonUtil.createBlackButton("手动刷新");
        Button resetButton = ButtonUtil.createBlackButton("重置");
        top.getChildren().addAll(searchInput, refreshButton, resetButton);

        TableView<MapleCharacter> center = createCenter();

        searchInput.setOnAction(event -> onRefresh(center, searchInput));
        refreshButton.setOnAction(event -> onRefresh(center, searchInput));
        resetButton.setOnAction(event -> {
            searchInput.setText(null);
            center.setItems(null);
        });
        regRefresh(center, searchInput);

        root.getChildren().addAll(top, new Separator(Orientation.HORIZONTAL), center);
        VBox.setVgrow(center, Priority.ALWAYS);
        setRoot(root);
    }

    @Override
    public void destroy() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        super.destroy();
    }

    private void onRefresh(TableView<MapleCharacter> center, TextField searchInput) {
        if (!Server.getInstance().isOnline()) {
            ConfirmBox.no(primaryStage, "当前服务尚未启动！");
            return;
        }
        doRefresh(center, searchInput);
    }

    private void regRefresh(TableView<MapleCharacter> center, TextField searchInput) {
        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (Server.getInstance().isOnline()) {
                    doRefresh(center, searchInput);
                }
            }
        }, 1000, 30000);
    }

    private synchronized void doRefresh(TableView<MapleCharacter> center, TextField searchInput) {
        String inputText = searchInput.getText();
        ObservableList<MapleCharacter> characters = FXCollections.observableArrayList();
        for (Channel channel : Server.getInstance().getAllChannels()) {
            for (MapleCharacter character : channel.getPlayerStorage().getAllCharacters()) {
                if (NapComUtils.isEmpty(inputText)) {
                    characters.add(character);
                } else if (String.valueOf(character.getAccountID()).contains(inputText) || character.getName().contains(inputText)) {
                    characters.add(character);
                }
            }
        }
        center.setItems(characters);
        center.refresh();
    }

    private TableView<MapleCharacter> createCenter() {
        TableView<MapleCharacter> tableView = new TableView<>();

        TableColumn<MapleCharacter, Number> accountColumn = new TableColumn<>("账号ID");
        tableView.getColumns().add(accountColumn);
        accountColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        accountColumn.setCellValueFactory(param -> new SimpleIntegerProperty(param.getValue().getAccountID()));

        TableColumn<MapleCharacter, String> nameColumn = new TableColumn<>("玩家名");
        tableView.getColumns().add(nameColumn);
        nameColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.2));
        nameColumn.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getName()));

        TableColumn<MapleCharacter, Number> mapColumn = new TableColumn<>("地图ID");
        tableView.getColumns().add(mapColumn);
        mapColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        mapColumn.setCellValueFactory(param -> new SimpleIntegerProperty(param.getValue().getMapId()));

        TableColumn<MapleCharacter, String> jobColumn = new TableColumn<>("职业");
        tableView.getColumns().add(jobColumn);
        jobColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        jobColumn.setCellValueFactory(param -> new SimpleStringProperty(Optional.ofNullable(MapleJob.getById(param.getValue().getJob().getId()))
                .orElse(MapleJob.BEGINNER).getName()));

        TableColumn<MapleCharacter, Number> levelColumn = new TableColumn<>("等级");
        tableView.getColumns().add(levelColumn);
        levelColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        levelColumn.setCellValueFactory(param -> new SimpleIntegerProperty(param.getValue().getLevel()));

        TableColumn<MapleCharacter, Number> gmColumn = new TableColumn<>("GM等级");
        tableView.getColumns().add(gmColumn);
        gmColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        gmColumn.setCellValueFactory(param -> new SimpleIntegerProperty(param.getValue().gmLevel()));

        TableColumn<MapleCharacter, Number> nxColumn = new TableColumn<>("点券");
        tableView.getColumns().add(nxColumn);
        nxColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.15));
        nxColumn.setCellValueFactory(param -> new SimpleIntegerProperty(param.getValue().getCashShop().getCash(1)));

        TableColumn<MapleCharacter, Number> mesoColumn = new TableColumn<>("金币");
        tableView.getColumns().add(mesoColumn);
        mesoColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.15));
        mesoColumn.setCellValueFactory(param -> new SimpleIntegerProperty(param.getValue().getMeso()));

        ContextMenu contextMenu = new ContextMenu();
        MenuItem nxItem = new MenuItem("发送点券");
        MenuItem expItem = new MenuItem("发送经验");
        MenuItem mesoItem = new MenuItem("发送金币");
        MenuItem gmItem = new MenuItem("设置GM等级");
        MenuItem itemItem = new MenuItem("发送道具");
        MenuItem equipItem = new MenuItem("发送装备");
        contextMenu.getItems().addAll(nxItem, expItem, mesoItem, gmItem, itemItem, equipItem);
        tableView.setContextMenu(contextMenu);

        nxItem.setOnAction(event -> sendAny(tableView, 0));
        expItem.setOnAction(event -> sendAny(tableView, 1));
        mesoItem.setOnAction(event -> sendAny(tableView, 2));
        gmItem.setOnAction(event -> sendAny(tableView, 3));
        itemItem.setOnAction(event -> {
            MapleCharacter character = tableView.getSelectionModel().getSelectedItem();
            if (character == null) {
                ConfirmBox.no(primaryStage, "请选中一个玩家");
                return;
            }
            String[] inputs = InputBox.getInputs(primaryStage, "请输入物品ID", "请输入物品数量");
            if (NapComUtils.isEmpty(inputs[0]) || !NapComUtils.isNumber(inputs[0])) {
                ConfirmBox.no(primaryStage, "请输入正确的物品ID");
                return;
            }
            int itemId = Integer.parseInt(inputs[0]);
            MapleInventoryType inventoryType = ItemConstants.getInventoryType(itemId);
            if (MapleInventoryType.EQUIP.equals(inventoryType)) {
                ConfirmBox.no(primaryStage, "不允许在此发送装备");
                return;
            }
            short quantity;
            if (NapComUtils.isEmpty(inputs[1])) {
                quantity = 1;
            } else if (inputs[1].length() > 3) {
                quantity = 999;
            } else {
                quantity = Short.parseShort(inputs[1]);
            }
            if (ItemConstants.isPet(itemId)) {
                long expire;
                String input = InputBox.getInput(primaryStage, "请输入过期时间(天)");
                if (NapComUtils.isEmpty(input)) {
                    expire = 90L * 24 * 60 * 60 * 1000;
                } else {
                    expire = Long.parseLong(input) * 24 * 60 * 60 * 1000;
                }
                character.getAbstractPlayerInteraction().gainItem(itemId, quantity, false, true, expire);
            } else {
                character.getAbstractPlayerInteraction().gainItem(itemId, quantity);
            }
            character.dropMessage(6, "管理员给你发送了" + quantity + "个" + "物品");
            MessageBox.showInfo(primaryStage, "发送成功");
        });
        equipItem.setOnAction(event -> {
            MapleCharacter character = tableView.getSelectionModel().getSelectedItem();
            if (character == null) {
                ConfirmBox.no(primaryStage, "请选中一个玩家");
                return;
            }
            String input = InputBox.getInput(primaryStage, "请输入装备ID");
            if (NapComUtils.isEmpty(input) || !NapComUtils.isNumber(input)) {
                ConfirmBox.no(primaryStage, "请输入正确的装备ID");
                return;
            }
            int equipId = Integer.parseInt(input);
            MapleInventoryType inventoryType = ItemConstants.getInventoryType(equipId);
            if (!MapleInventoryType.EQUIP.equals(inventoryType)) {
                ConfirmBox.no(primaryStage, "请输入装备ID");
                return;
            }
            Equip equip = MapleItemInformationProvider.getInstance().getNullableEquip(equipId);
            if (equip == null) {
                ConfirmBox.no(primaryStage, "装备不存在");
                return;
            }
            List<String> texts = new ArrayList<>();
            List<String> defaults = new ArrayList<>();
            buildEquipDefaults(equip, texts, defaults);
            String[] inputs = InputBox.getInputs(primaryStage, texts, defaults);
            if (NapComUtils.isEmpty(inputs[0])) {
                MessageBox.showInfo(primaryStage, "取消发送");
                return;
            }
            character.getAbstractPlayerInteraction().gainEquip(equipId, toShort(inputs[0]), toShort(inputs[1]),
                    toShort(inputs[2]), toShort(inputs[3]), toShort(inputs[4]), toShort(inputs[5]), toShort(inputs[6]),
                    toShort(inputs[7]), toShort(inputs[8]), toShort(inputs[9]), toShort(inputs[10]), toShort(inputs[11]),
                    toShort(inputs[12]), toShort(inputs[13]), -1L, (short) 1);
            character.dropMessage(6, "管理员给你发送了1件装备");
            MessageBox.showInfo(primaryStage, "发送成功");
        });
        return tableView;
    }

    private void sendAny(TableView<MapleCharacter> tableView, int type) {
        MapleCharacter character = tableView.getSelectionModel().getSelectedItem();
        if (character == null) {
            ConfirmBox.no(primaryStage, "请选中一个玩家");
            return;
        }
        String[][] messages = {
                {"发送点券", "请输入正确的点券数量", "点券发送完成"},
                {"发送经验", "请输入正确的经验数量", "经验发送完成"},
                {"发送金币", "请输入正确的金币数量", "金币发送完成"},
                {"设置GM等级", "请输入正确的GM等级", "GM等级设置完成"}
        };
        String input = InputBox.getInput(primaryStage, messages[type][0]);
        if (input == null) {
            return;
        }
        if (!NapComUtils.isNumber(input)) {
            MessageBox.showError(primaryStage, messages[type][1]);
            return;
        }
        if (input.length() > 32 || Long.parseLong(input) > Integer.MAX_VALUE) {
            MessageBox.showError(primaryStage, "输入的值不能大于" + Integer.MAX_VALUE);
            return;
        }
        int inputNum = Integer.parseInt(input);
        if (type == 0) {
            character.getCashShop().gainNx(inputNum);
            character.startMapEffect("管理员给你发送了" + inputNum + "点券！", 5120015);
        } else if (type == 1) {
            character.gainExp(inputNum);
            character.startMapEffect("管理员给你发送了" + inputNum + "经验！", 5120015);
        } else if (type == 2) {
            character.gainMeso(inputNum);
            character.startMapEffect("管理员给你发送了" + inputNum + "金币！", 5120015);
        } else {
            if (inputNum < 3) {
                character.Hide(false);
                character.setGMLevel(inputNum);
            } else {
                character.setGMLevel(inputNum);
                character.Hide(true);
            }
            character.dropMessage(6, "管理员已将你的GM等级设置为[" + inputNum + "]");
        }
        MessageBox.showInfo(primaryStage, messages[type][2]);
    }

    private void buildEquipDefaults(Equip equip, List<String> texts, List<String> defaults) {
        texts.add("力量");
        defaults.add(String.valueOf(equip.getStr()));
        texts.add("敏捷");
        defaults.add(String.valueOf(equip.getDex()));
        texts.add("智力");
        defaults.add(String.valueOf(equip.getInt()));
        texts.add("运气");
        defaults.add(String.valueOf(equip.getLuk()));
        texts.add("血量");
        defaults.add(String.valueOf(equip.getHp()));
        texts.add("蓝量");
        defaults.add(String.valueOf(equip.getMp()));
        texts.add("物理攻击");
        defaults.add(String.valueOf(equip.getWatk()));
        texts.add("魔法攻击");
        defaults.add(String.valueOf(equip.getMatk()));
        texts.add("物理防御");
        defaults.add(String.valueOf(equip.getWdef()));
        texts.add("魔法防御");
        defaults.add(String.valueOf(equip.getMdef()));
        texts.add("命中");
        defaults.add(String.valueOf(equip.getAcc()));
        texts.add("回避");
        defaults.add(String.valueOf(equip.getAvoid()));
        texts.add("移速");
        defaults.add(String.valueOf(equip.getSpeed()));
        texts.add("跳跃");
        defaults.add(String.valueOf(equip.getJump()));
    }

    private Short toShort(String input) {
        return NapComUtils.isEmpty(input) ? null : Short.parseShort(input);
    }
}

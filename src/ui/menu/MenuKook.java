package ui.menu;

import cn.nap.utils.common.NapComUtils;
import com.sun.javafx.application.PlatformImpl;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Pagination;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import net.server.Server;
import tools.Pair;
import ui.component.ConfirmBox;
import ui.component.InputBox;
import ui.component.MessageBox;
import ui.component.ProgressLoading;
import ui.dao.KookConfigDao;
import ui.dao.KookMessageDao;
import ui.manager.KookManager;
import ui.model.KookMessage;
import ui.model.KookProp;
import ui.util.ButtonUtil;
import ui.util.ViewStyle;

import java.util.ArrayList;
import java.util.List;

public class MenuKook extends MenuAbstract {
    @Override
    protected void show() {
        VBox root = new VBox();
        root.setSpacing(5);

        HBox top = new HBox();
        top.setAlignment(Pos.CENTER_LEFT);
        top.setSpacing(5);
        ComboBox<String> statusComoBox = new ComboBox<>(FXCollections.observableArrayList("未绑定", "已绑定"));
        statusComoBox.setPromptText("请选择绑定状态");
        statusComoBox.setStyle("-fx-background-color: transparent;-fx-border-color: gray;-fx-border-width: 1;-fx-border-radius: 5");
        Button searchButton = ButtonUtil.createBlackButton("查询");
        Button resetSearchButton = ButtonUtil.createBlackButton("重置");
        Button configButton = ButtonUtil.createGreenButton("kook配置");
        Button onlineButton;
        if (KookManager.getInstance().isOnline()) {
            onlineButton = ButtonUtil.createRedButton("下线机器人");
        } else {
            onlineButton = ButtonUtil.createGreenButton("上线机器人");
        }
        ViewStyle onlineStyle = new ViewStyle(onlineButton);
        top.getChildren().addAll(statusComoBox, searchButton, resetSearchButton, configButton, onlineButton);

        Pagination center = new Pagination(1);
        center.setPageFactory(index -> createTableView());

        searchButton.setOnAction(event -> onSearch(center, statusComoBox));
        resetSearchButton.setOnAction(event -> {
            statusComoBox.getSelectionModel().select(-1);
            center.setPageFactory(null);
            center.setPageCount(1);
        });
        configButton.setOnAction(event -> {
            KookProp kookProp = KookConfigDao.readKookProp();
            Pair<List<String>, List<String>> pair = buildTexts(kookProp);
            String[] inputs = InputBox.getInputs(primaryStage, pair.getLeft(), pair.getRight());
            if (InputBox.isNo(inputs)) {
                return;
            }
            buildKookProp(kookProp, inputs);
            KookConfigDao.updateKookProp(kookProp);
            MessageBox.showInfo(primaryStage, "更新成功");
        });
        onlineButton.setOnAction(event -> {
            KookProp kookProp = KookConfigDao.readKookProp();
            if (notPassCheck(kookProp)) {
                PlatformImpl.runAndWait(() -> onlineButton.setDisable(false));
                return;
            }
            PlatformImpl.runAndWait(() -> onlineButton.setDisable(true));
            if (KookManager.getInstance().isOnline()) {
                KookManager.getInstance().offline();
            } else {
                KookManager.getInstance().online(kookProp);
            }
            if (KookManager.getInstance().isOnline()) {
                onlineButton.setText("下线机器人");
                onlineStyle.updateAndSet("-fx-background-color: red");
            } else {
                onlineButton.setText("上线机器人");
                onlineStyle.updateAndSet("-fx-background-color: green");
            }
            PlatformImpl.runAndWait(() -> onlineButton.setDisable(false));
        });

        root.getChildren().addAll(top, new Separator(Orientation.HORIZONTAL), center);
        VBox.setVgrow(center, Priority.ALWAYS);
        setRoot(root);
    }

    private void onSearch(Pagination center, ComboBox<String> statusComoBox) {
        int selectedIndex = statusComoBox.getSelectionModel().getSelectedIndex();
        ProgressLoading loading = new ProgressLoading(primaryStage);
        final int PAGE_SIZE = 20;
        loading.add("正在查询总页数", () -> {
            long count = KookMessageDao.selectCountByCondition(selectedIndex);
            PlatformImpl.runAndWait(() -> center.setPageCount((int) Math.ceil((double) count / PAGE_SIZE)));
        });
        loading.execute();
        center.setPageFactory(index -> {
            TableView<KookMessage> tableView = createTableView();

            ProgressLoading searchLoading = new ProgressLoading(primaryStage);
            searchLoading.add("正在分页查询配置", () -> {
                ObservableList<KookMessage> commands = FXCollections.observableArrayList();
                commands.addAll(KookMessageDao.selectPageByCondition(index, PAGE_SIZE, selectedIndex));
                PlatformImpl.runAndWait(() -> tableView.setItems(commands));
            });
            searchLoading.execute();
            return tableView;
        });
    }

    private TableView<KookMessage> createTableView() {
        TableView<KookMessage> tableView = new TableView<>();
        tableView.setEditable(true);
        tableView.setStyle("-fx-font-size: 14");

        TableColumn<KookMessage, Number> accountColumn = new TableColumn<>("账号ID");
        tableView.getColumns().add(accountColumn);
        accountColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        accountColumn.setCellValueFactory(new PropertyValueFactory<>("accountId"));

        TableColumn<KookMessage, String> receiveColumn = new TableColumn<>("原始绑定信息");
        tableView.getColumns().add(receiveColumn);
        receiveColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.4));
        receiveColumn.setCellValueFactory(new PropertyValueFactory<>("receiveContent"));

        TableColumn<KookMessage, String> replyColumn = new TableColumn<>("原始回复信息");
        tableView.getColumns().add(replyColumn);
        replyColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.4));
        replyColumn.setCellValueFactory(new PropertyValueFactory<>("replyContent"));

        TableColumn<KookMessage, String> codeColumn = new TableColumn<>("绑定码");
        tableView.getColumns().add(codeColumn);
        codeColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        codeColumn.setCellValueFactory(new PropertyValueFactory<>("bindCode"));

        return tableView;
    }

    private Pair<List<String>, List<String>> buildTexts(KookProp kookProp) {
        List<String> texts = new ArrayList<>();
        List<String> defaults = new ArrayList<>();

        texts.add("机器人Token");
        defaults.add(kookProp.getBotToken());

        texts.add("服务器ID");
        defaults.add(kookProp.getGuildId());

        texts.add("服务器名称");
        defaults.add(kookProp.getGuildName());

        texts.add("绑定频道ID");
        defaults.add(kookProp.getChannelId());

        texts.add("绑定频道名称");
        defaults.add(kookProp.getChannelName());

        texts.add("绑定关键字");
        defaults.add(kookProp.getBindKeywords());
        return new Pair<>(texts, defaults);
    }

    private void buildKookProp(KookProp kookProp, String[] inputs) {
        kookProp.setBotToken(inputs[0]);
        kookProp.setGuildId(inputs[1]);
        kookProp.setGuildName(inputs[2]);
        kookProp.setChannelId(inputs[3]);
        kookProp.setChannelName(inputs[4]);
        kookProp.setBindKeywords(inputs[5]);
    }

    public boolean notPassCheck(KookProp kookProp) {
        if (!Server.getInstance().isOnline()) {
            ConfirmBox.no(primaryStage, "当前服务尚未启动");
            return true;
        }
        if (NapComUtils.isEmpty(kookProp.getBotToken())) {
            MessageBox.showError(primaryStage, "机器人Token未配置");
            return true;
        }
        if (NapComUtils.isEmpty(kookProp.getGuildId()) && NapComUtils.isEmpty(kookProp.getGuildName())) {
            MessageBox.showError(primaryStage, "服务器id和服务器名称不能同时为空");
            return true;
        }
        if (NapComUtils.isEmpty(kookProp.getChannelId()) && NapComUtils.isEmpty(kookProp.getChannelName())) {
            MessageBox.showError(primaryStage, "绑定频道id和绑定频道名称不能同时为空");
            return true;
        }
        if (NapComUtils.isEmpty(kookProp.getBindKeywords())) {
            MessageBox.showError(primaryStage, "绑定关键词不能为空");
            return true;
        }
        return false;
    }
}

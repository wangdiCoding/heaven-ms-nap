package ui.menu;

import client.MapleCharacter;
import cn.nap.utils.common.NapComUtils;
import com.sun.javafx.application.PlatformImpl;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import net.server.Server;
import net.server.channel.Channel;
import net.server.world.World;
import tools.MaplePacketCreator;
import ui.component.ConfirmBox;
import ui.component.InputBox;
import ui.component.MessageBox;
import ui.component.ProgressLoading;
import ui.component.SearchItemBox;
import ui.util.ButtonUtil;

public class MenuPopular extends MenuAbstract {
    @Override
    protected void show() {
        TilePane root = new TilePane();
        root.setVgap(10);
        root.setHgap(10);
        Button reloadEventButton = ButtonUtil.createPopularButton("重载事件");
        Button sendWorldMsgButton = ButtonUtil.createPopularButton("发送全服公告");
        Button sendWorldNxButton = ButtonUtil.createPopularButton("发送全服点券");
        Button sendWorldExpButton = ButtonUtil.createPopularButton("发送全服经验");
        Button sendWorldMesoButton = ButtonUtil.createPopularButton("发送全服金币");
        Button searchItemButton = ButtonUtil.createPopularButton("查询物品");
        root.getChildren().addAll(reloadEventButton, sendWorldMsgButton, sendWorldNxButton, sendWorldExpButton,
                sendWorldMesoButton, searchItemButton);

        reloadEventButton.setOnAction(event -> {
            if (!check()) {
                return;
            }
            ProgressLoading loading = new ProgressLoading(primaryStage);
            loading.add("正在重载事件...", () -> {
                for (Channel ch : Server.getInstance().getAllChannels()) {
                    if (ch.getEventSM() == null) {
                        PlatformImpl.runAndWait(() -> MessageBox.showInfo(primaryStage, "事件尚未初始化完成，请稍后重试！"));
                        return;
                    }
                    ch.reloadEventScriptManager();
                }
                PlatformImpl.runAndWait(() -> MessageBox.showInfo(primaryStage, "事件已重载完成！"));
            });
            loading.execute();
        });
        sendWorldMsgButton.setOnAction(event -> {
            if (!check()) {
                return;
            }
            String input = InputBox.getInput(primaryStage, "请输入公告内容");
            if (input == null) {
                return;
            }
            ProgressLoading loading = new ProgressLoading(primaryStage);
            loading.add("正在发送公告...", () -> {
                for (World world : Server.getInstance().getWorlds()) {
                    world.broadcastPacket(MaplePacketCreator.serverNotice(0, input));
                }
                PlatformImpl.runAndWait(() -> MessageBox.showInfo(primaryStage, "公告发送完成！"));
            });
            loading.execute();
        });
        sendWorldNxButton.setOnAction(event -> sendAny(0));
        sendWorldExpButton.setOnAction(event -> sendAny(1));
        sendWorldMesoButton.setOnAction(event -> sendAny(2));
        searchItemButton.setOnAction(event -> {
            if (!check()) {
                return;
            }
            new SearchItemBox(primaryStage).show();
        });

        setRoot(root);
    }

    private boolean check() {
        if (!Server.getInstance().isOnline()) {
            ConfirmBox.no(primaryStage, "当前服务尚未启动！");
            return false;
        }
        return true;
    }

    private void sendAny(int type) {
        if (!check()) {
            return;
        }
        String input = InputBox.getInput(primaryStage, type == 0 ? "发送点券" : (type == 1 ? "发送经验" : "发送金币"));
        if (input == null) {
            return;
        }
        if (!NapComUtils.isNumber(input)) {
            MessageBox.showError(primaryStage, "请输入正确的" + (type == 0 ? "点券" : (type == 1 ? "经验" : "金币")) + "数量！");
            return;
        }
        if (input.length() > 32 || Long.parseLong(input) > Integer.MAX_VALUE) {
            MessageBox.showError(primaryStage, "输入的值不能大于" + Integer.MAX_VALUE);
            return;
        }
        int inputNum = Integer.parseInt(input);
        ProgressLoading loading = new ProgressLoading(primaryStage);
        loading.add("正在发送点券...", () -> {
            for (Channel channel : Server.getInstance().getAllChannels()) {
                for (MapleCharacter character : channel.getPlayerStorage().getAllCharacters()) {
                    if (type == 0) {
                        character.getCashShop().gainNx(inputNum);
                        character.startMapEffect("管理员给大家发送了" + inputNum + "点券！", 5120015);
                    } else if (type == 1) {
                        character.gainExp(inputNum);
                        character.startMapEffect("管理员给大家发送了" + inputNum + "经验！", 5120015);
                    } else {
                        character.gainMeso(inputNum);
                        character.startMapEffect("管理员给大家发送了" + inputNum + "金币！", 5120015);
                    }
                }
            }
            PlatformImpl.runAndWait(() -> MessageBox.showInfo(primaryStage, (type == 0 ? "点券" : (type == 1 ? "经验" : "金币")) + "发送完成！"));
        });
        loading.execute();
    }
}

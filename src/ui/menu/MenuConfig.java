package ui.menu;

import cn.nap.constant.NapPunc;
import cn.nap.utils.common.NapComUtils;
import com.sun.javafx.application.PlatformImpl;
import config.CommonConfig;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import tools.Log;
import ui.component.ConfirmBox;
import ui.component.MessageBox;
import ui.component.ProgressLoading;
import ui.component.TextInput;
import ui.component.TextSwitch;
import ui.constant.ConfigType;
import ui.constant.DataType;
import ui.dao.ConfigServerDao;
import ui.model.ConfigServer;
import ui.util.ButtonUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MenuConfig extends MenuAbstract {
    private final AtomicBoolean isPopular = new AtomicBoolean(true);
    private final List<ConfigServer> changeList = new ArrayList<>();

    @Override
    protected void show() {
        VBox root = new VBox();
        root.setSpacing(5);
        HBox top = new HBox();
        top.setAlignment(Pos.CENTER_LEFT);
        top.setSpacing(5);
        root.getChildren().addAll(top, new Separator(Orientation.HORIZONTAL));
        if (isPopular.get()) {
            showPopular(root, top);
        } else {
            showAll(root, top);
        }
        setRoot(root);
    }

    private void showPopular(VBox root, HBox top) {
        Button saveButton = ButtonUtil.createGreenButton("保存");
        top.getChildren().add(saveButton);
        addChange(top);

        ProgressLoading loading = new ProgressLoading(primaryStage);
        List<ConfigServer> configServers = new ArrayList<>();
        VBox bottom = new VBox();
        loading.add("正在查询常用配置", () -> configServers.addAll(ConfigServerDao.selectPopular()));
        loading.add("正在展示常用配置", () -> {
            if (NapComUtils.isEmpty(configServers)) {
                return;
            }
            Map<String, List<ConfigServer>> configTypeMap = configServers.stream().collect(Collectors.groupingBy(ConfigServer::getConfigType));
            for (Map.Entry<String, List<ConfigServer>> entry : configTypeMap.entrySet()) {
                if (NapComUtils.isEmpty(entry.getKey())) {
                    continue;
                }
                Label typeLabel = new Label(ConfigType.ofType(entry.getKey()).getDesc());
                typeLabel.setStyle("-fx-font-size: 14;-fx-font-weight: bold;-fx-padding: 30 0 10 0");
                TilePane inputTilePane = new TilePane();
                TilePane switchTilePane = new TilePane();

                for (ConfigServer configServer : entry.getValue()) {
                    Tooltip tooltip = new Tooltip(configServer.getConfigDesc());
                    String text = Optional.ofNullable(configServer.getSimpleName()).orElse(configServer.getConfigName());
                    if ("Boolean".equals(configServer.getConfigDataType())) {
                        TextSwitch textSwitch = new TextSwitch(text);
                        textSwitch.getSwitchButton().setOnOff("true".equals(configServer.getConfigData()));
                        textSwitch.setPadding(new Insets(0, 20, 10, 0));
                        switchTilePane.getChildren().add(textSwitch);
                        Tooltip.install(textSwitch, tooltip);
                        textSwitch.getSwitchButton().onOffProperty().addListener((observable, oldValue, newValue) -> {
                            if (newValue == null) {
                                return;
                            }
                            if (newValue.equals("true".equals(configServer.getConfigData()))) {
                                changeList.removeIf(tmp -> NapComUtils.equals(configServer.getId(), tmp.getId()));
                                return;
                            }
                            ConfigServer clone = configServer.clone();
                            clone.setConfigData(newValue ? "true" : "false");
                            changeList.add(clone);
                        });
                    } else {
                        TextInput textInput = new TextInput(text);
                        textInput.getTextField().setText(configServer.getConfigData());
                        textInput.setPadding(new Insets(0, 20, 10, 0));
                        inputTilePane.getChildren().add(textInput);
                        Tooltip.install(textInput, tooltip);
                        textInput.getTextField().textProperty().addListener((observable, oldValue, newValue) -> {
                            if (NapComUtils.isEmpty(newValue)) {
                                return;
                            }
                            if (NapComUtils.equals(newValue, configServer.getConfigData())) {
                                changeList.removeIf(tmp -> NapComUtils.equals(configServer.getId(), tmp.getId()));
                                return;
                            }
                            ConfigServer clone = configServer.clone();
                            clone.setConfigData(newValue);
                            changeList.add(clone);
                        });
                    }
                }
                PlatformImpl.runAndWait(() -> bottom.getChildren().addAll(typeLabel, inputTilePane, switchTilePane));
            }
        });
        loading.execute();

        saveButton.setOnAction(event -> onSave(configServers));
        root.getChildren().add(bottom);
        VBox.setVgrow(bottom, Priority.ALWAYS);
    }

    private void showAll(VBox root, HBox top) {

        TextField searchTextField = new TextField();
        searchTextField.setPromptText("请输入配置名/描述");
        Button searchButton = ButtonUtil.createBlackButton("搜索");
        Button resetButton = ButtonUtil.createBlackButton("重置");
        Button saveButton = ButtonUtil.createGreenButton("保存");
        top.getChildren().addAll(searchTextField, searchButton, resetButton, saveButton);
        addChange(top);

        Pagination bottom = new Pagination(1);
        bottom.setPageFactory(index -> createTableView());

        List<ConfigServer> oldConfigServers = new ArrayList<>();
        searchTextField.setOnAction(event -> onSearch(searchTextField, bottom, oldConfigServers));
        searchButton.setOnAction(event -> onSearch(searchTextField, bottom, oldConfigServers));
        resetButton.setOnAction(event -> {
            searchTextField.setText(null);
            bottom.setPageFactory(index -> createTableView());
            bottom.setPageCount(1);
            changeList.clear();
            oldConfigServers.clear();
        });
        saveButton.setOnAction(event -> onSave(oldConfigServers));

        root.getChildren().add(bottom);
        VBox.setVgrow(bottom, Priority.ALWAYS);
    }

    private void addChange(HBox top) {
        Region spacer = new Region();
        Label label = new Label(isPopular.get() ? "常用配置" : "全部配置");
        label.setStyle("-fx-font-size: 14;-fx-font-weight:bold;-fx-text-fill: linear-gradient(from 0% 0% to 100% 100%, blueviolet, deeppink)");
        Button changeButton = ButtonUtil.createBlackButton("切换配置");
        top.getChildren().addAll(spacer, label, changeButton);
        HBox.setHgrow(spacer, Priority.ALWAYS);
        changeButton.setOnAction(event -> {
            destroy();
            isPopular.set(!isPopular.get());
            changeList.clear();
            show();
        });
    }

    private TableView<ConfigServer> createTableView() {
        TableView<ConfigServer> tableView = new TableView<>();
        tableView.setEditable(true);
        tableView.setStyle("-fx-font-size: 14");

        TableColumn<ConfigServer, String> worldColumn = new TableColumn<>("配置范围");
        tableView.getColumns().add(worldColumn);
        worldColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        worldColumn.setCellValueFactory(param -> NapPunc.ASTERISK.equals(param.getValue().getConfigWorld())
                ? new SimpleStringProperty("全局")
                : new SimpleStringProperty("大区" + param.getValue().getConfigWorld()));

        TableColumn<ConfigServer, String> typeColumn = new TableColumn<>("配置类型");
        tableView.getColumns().add(typeColumn);
        typeColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        typeColumn.setCellValueFactory(param -> new SimpleStringProperty(ConfigType.ofType(param.getValue().getConfigType()).getDesc()));

        TableColumn<ConfigServer, String> nameColumn = new TableColumn<>("配置名");
        tableView.getColumns().add(nameColumn);
        nameColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("configName"));

        TableColumn<ConfigServer, String> dataColumn = new TableColumn<>("配置值");
        tableView.getColumns().add(dataColumn);
        dataColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.2));
        dataColumn.setCellValueFactory(new PropertyValueFactory<>("configData"));
        dataColumn.setCellFactory(TextFieldTableCell.forTableColumn());

        TableColumn<ConfigServer, String> dataTypeColumn = new TableColumn<>("配置值类型");
        tableView.getColumns().add(dataTypeColumn);
        dataTypeColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        dataTypeColumn.setCellValueFactory(param -> new SimpleStringProperty(DataType.ofType(param.getValue().getConfigDataType()).getDesc()));

        TableColumn<ConfigServer, String> descColumn = new TableColumn<>("配置描述");
        tableView.getColumns().add(descColumn);
        descColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.3));
        descColumn.setCellValueFactory(new PropertyValueFactory<>("configDesc"));

        TableColumn<ConfigServer, String> popularColumn = new TableColumn<>("是否常用");
        tableView.getColumns().add(popularColumn);
        popularColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        popularColumn.setCellValueFactory(param -> new SimpleStringProperty("true".equals(param.getValue().getPopular()) ? "是" : "否"));
        popularColumn.setCellFactory(ComboBoxTableCell.forTableColumn(FXCollections.observableArrayList("是", "否")));

        TableColumn<ConfigServer, String> simpleColumn = new TableColumn<>("配置简称");
        tableView.getColumns().add(simpleColumn);
        simpleColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        simpleColumn.setCellValueFactory(new PropertyValueFactory<>("simpleName"));
        simpleColumn.setCellFactory(TextFieldTableCell.forTableColumn());

        return tableView;
    }

    private void onSearch(TextField searchTextField, Pagination bottom, List<ConfigServer> oldConfigServers) {
        String searchText = searchTextField.getText();
        ProgressLoading loading = new ProgressLoading(primaryStage);
        final int PAGE_SIZE = 20;
        loading.add("正在查询总页数", () -> {
            int count = ConfigServerDao.selectUiCount(searchText);
            PlatformImpl.runAndWait(() -> {
                int pageCount = count / PAGE_SIZE;
                if (count % PAGE_SIZE > 0) {
                    pageCount++;
                }
                bottom.setPageCount(pageCount);
            });

        });
        loading.execute();
        bottom.setPageFactory(index -> {
            ProgressLoading searchLoading = new ProgressLoading(primaryStage);
            ObservableList<ConfigServer> configServers = FXCollections.observableArrayList();
            searchLoading.add("正在分页查询配置", () -> {
                // 每次翻页都清空changeList
                changeList.clear();
                oldConfigServers.clear();
                oldConfigServers.addAll(ConfigServerDao.selectUiList(searchText, index, PAGE_SIZE));
                oldConfigServers.forEach(configServer -> configServers.add(configServer.clone()));
            });
            searchLoading.execute();

            TableView<ConfigServer> tableView = createTableView();
            for (TableColumn<ConfigServer, ?> column : tableView.getColumns()) {
                Function<ConfigServer, String> getterMapper = getterMapper(column.getText());
                BiConsumer<ConfigServer, String> setterMapper = setterMapper(column.getText());
                if (getterMapper == null || setterMapper == null) {
                    continue;
                }
                column.setOnEditCommit(event -> {
                    String newValue = String.valueOf(event.getNewValue());
                    int row = event.getTablePosition().getRow();
                    ConfigServer oldConfig = oldConfigServers.get(row);
                    ConfigServer editConfig = configServers.get(row);
                    setterMapper.accept(editConfig, newValue);
                    if (notChange(editConfig, oldConfig)) {
                        changeList.removeIf(change -> NapComUtils.equals(change.getId(), editConfig.getId()));
                        return;
                    }
                    changeList.add(editConfig);
                });
            }

            tableView.setItems(configServers);
            return tableView;
        });
    }

    private void onSave(List<ConfigServer> configServers) {
        if (!ConfirmBox.both(primaryStage, "确认进行保存吗？")) {
            return;
        }
        if (NapComUtils.isEmpty(changeList)) {
            return;
        }
        try {
            ConfigServerDao.updateList(changeList);
            MessageBox.showInfo(primaryStage, "保存成功！");
        } catch (Exception e) {
            MessageBox.showError(primaryStage, "保存失败！");
            Log.error("保存常用配置异常", e);
            return;
        }
        configServers.forEach(configServer -> changeList.stream()
                .filter(change -> NapComUtils.equals(configServer.getId(), change.getId()))
                .findFirst()
                .ifPresent(server -> configServer.setConfigData(server.getConfigData())));
        ProgressLoading loading = new ProgressLoading(primaryStage);
        loading.add("正在重载更改的配置...", () -> {
            try {
                CommonConfig.config.reloadServerConfig(changeList);
            } catch (Exception e) {
                PlatformImpl.runAndWait(() -> ConfirmBox.yes(primaryStage, "重载配置失败，请检查配置是否正确！"));
                Log.error("重载配置失败：", e);
            } finally {
                changeList.clear();
            }
        });
        loading.execute();
    }

    private boolean notChange(ConfigServer editConfig, ConfigServer oldConfig) {
        return NapComUtils.equals(editConfig.getConfigData(), oldConfig.getConfigData())
                && NapComUtils.equals(editConfig.getPopular(), oldConfig.getPopular())
                && NapComUtils.equals(editConfig.getSimpleName(), NapComUtils.isEmpty(oldConfig.getSimpleName()) ? null : oldConfig.getSimpleName());
    }

    private Function<ConfigServer, String> getterMapper(String text) {
        switch (text) {
            case "配置值":
                return ConfigServer::getConfigData;
            case "是否常用":
                return configServer -> "true".equals(configServer.getPopular()) ? "是" : "否";
            case "配置简称":
                return ConfigServer::getSimpleName;
            default:
                return null;
        }
    }

    private BiConsumer<ConfigServer, String> setterMapper(String text) {
        switch (text) {
            case "配置值":
                return ConfigServer::setConfigData;
            case "是否常用":
                return (configServer, value) -> configServer.setPopular("是".equals(value) ? "true" : "false");
            case "配置简称":
                return ConfigServer::setSimpleName;
            default:
                return null;
        }
    }
}

package ui.menu;

import cn.nap.utils.common.NapComUtils;
import com.sun.javafx.application.PlatformImpl;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Pagination;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import net.server.Server;
import server.cashshop.CashShop;
import ui.component.ConfirmBox;
import ui.component.MessageBox;
import ui.component.NumberTableCell;
import ui.component.ProgressLoading;
import ui.component.SwitchButton;
import ui.dao.CashItemDao;
import ui.util.ButtonUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MenuMall extends MenuAbstract {
    @Override
    protected void show() {
        VBox root = new VBox();
        root.setStyle("-fx-spacing: 5");

        HBox top = new HBox();
        top.setStyle("-fx-spacing: 5");
        TextField searchInput = new TextField();
        searchInput.setPromptText("请输入物品SN/ID");
        Button searchButton = ButtonUtil.createBlackButton("搜索");
        Button resetButton = ButtonUtil.createBlackButton("重置");
        Button saveButton = ButtonUtil.createGreenButton("保存");
        Button initButton = ButtonUtil.createGreenButton("初始化物品");
        top.getChildren().addAll(searchInput, searchButton, resetButton, saveButton, initButton);

        List<CashShop.CashItem> oriList = new ArrayList<>();
        List<CashShop.CashItem> changeList = new ArrayList<>();

        Pagination center = new Pagination(1);
        center.setPageFactory(index -> createTableView(oriList, changeList));
        searchButton.setOnAction(event -> onSearch(center, searchInput, oriList, changeList));
        searchInput.setOnAction(event -> onSearch(center, searchInput, oriList, changeList));
        resetButton.setOnAction(event -> {
            center.setPageFactory(null);
            center.setPageCount(1);
            searchInput.setText(null);
        });
        saveButton.setOnAction(event -> {
            if (!Server.getInstance().isOnline()) {
                ConfirmBox.no(primaryStage, "当前服务尚未启动！");
                return;
            }
            if (!ConfirmBox.both(primaryStage, "确认进行保存吗？")) {
                return;
            }
            ProgressLoading loading = new ProgressLoading(primaryStage);
            List<CashShop.CashItem> readyList = new ArrayList<>();
            loading.add("正在校验物品信息...", () -> {
                Map<Integer, CashShop.CashItem> customerItems = CashShop.CashItemFactory.getCustomerItems();
                if (NapComUtils.isEmpty(customerItems)) {
                    readyList.addAll(changeList.stream().filter(CashShop.CashItem::isOnSale).collect(Collectors.toList()));
                    return;
                }
                for (Map.Entry<Integer, CashShop.CashItem> entry : customerItems.entrySet()) {
                    CashShop.CashItem clone = entry.getValue().clone();
                    changeList.stream().filter(change -> NapComUtils.equals(change.getSn(), clone.getSn()))
                            .findFirst()
                            .ifPresent(change -> {
                                clone.setCount(change.getCount());
                                clone.setPrice(change.getPrice());
                                clone.setPriority(change.getPriority());
                                clone.setPeriod(change.getPeriod());
                                clone.setOnSale(change.getOnSale());
                            });
                    if (clone.isOnSale()) {
                        readyList.add(clone);
                    }
                }
            });
            loading.add("正在保存物品信息...", () -> {
                if (NapComUtils.isEmpty(readyList)) {
                    PlatformImpl.runAndWait(() -> MessageBox.showInfo(primaryStage, "保存成功"));
                    return;
                }
                // 发给客户端的包最大5万个字节，超过客户端崩溃，所以这里限制最大只能开启1800个物品
                if (readyList.size() > 1800) {
                    PlatformImpl.runAndWait(() -> MessageBox.showError(primaryStage, "开启销售的物品不能超过1800个"));
                    return;
                }
                CashItemDao.updateList(readyList);
                Map<Integer, CashShop.CashItem> customerItems = CashShop.CashItemFactory.getCustomerItems();
                customerItems.clear();
                readyList.forEach(cashItem -> customerItems.put(cashItem.getSn(), cashItem));
                oriList.forEach(oriItem -> changeList.stream()
                        .filter(change -> NapComUtils.equals(change.getSn(), oriItem.getSn()))
                        .findFirst()
                        .ifPresent(change -> {
                            oriItem.setCount(change.getCount());
                            oriItem.setPrice(change.getPrice());
                            oriItem.setPriority(change.getPriority());
                            oriItem.setPeriod(change.getPeriod());
                            oriItem.setOnSale(change.getOnSale());
                        }));
                changeList.clear();
                PlatformImpl.runAndWait(() -> MessageBox.showInfo(primaryStage, "保存成功"));
            });
            loading.execute();
        });
        initButton.setOnAction(event -> {
            if (!Server.getInstance().isOnline()) {
                ConfirmBox.no(primaryStage, "当前服务尚未启动！");
                return;
            }
            boolean choice = ConfirmBox.both(primaryStage, "初始化物品：\n" +
                    "1.从Commodity.img.xml中读取OnSale为0的物品，请确保服务端的wz是否正确\n" +
                    "2.该操作进行后，会清空之前所有操作的数据，相当于重置\n\n" +
                    "确认继续吗？");
            if (!choice) {
                return;
            }
            ProgressLoading loading = new ProgressLoading(primaryStage);
            List<CashShop.CashItem> cashItems = new ArrayList<>();
            loading.add("正在读取物品信息...", () -> {
                Map<Integer, CashShop.CashItem> items = CashShop.CashItemFactory.getItems();
                items.values().forEach(item -> {
                    if (!item.isOnSale()) {
                        cashItems.add(item);
                    }
                });
            });
            loading.add("正在初始化物品信息...", () -> {
                CashItemDao.deleteAll();
                CashItemDao.insert(cashItems);
            });
            loading.add("正在刷新物品信息...", () -> PlatformImpl.runAndWait(() -> onSearch(center, searchInput, oriList, changeList)));
            loading.execute();
        });

        root.getChildren().addAll(top, new Separator(Orientation.HORIZONTAL), center);
        VBox.setVgrow(center, Priority.ALWAYS);
        setRoot(root);
    }

    private TableView<CashShop.CashItem> createTableView(List<CashShop.CashItem> oriList, List<CashShop.CashItem> changeList) {
        TableView<CashShop.CashItem> tableView = new TableView<>();
        tableView.setEditable(true);
        tableView.setStyle("-fx-font-size: 14");

        TableColumn<CashShop.CashItem, Number> snColumn = new TableColumn<>("SN码");
        tableView.getColumns().add(snColumn);
        snColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.15));
        snColumn.setCellValueFactory(new PropertyValueFactory<>("sn"));

        TableColumn<CashShop.CashItem, Number> idColumn = new TableColumn<>("物品ID");
        tableView.getColumns().add(idColumn);
        idColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.15));
        idColumn.setCellValueFactory(new PropertyValueFactory<>("itemId"));

        TableColumn<CashShop.CashItem, Number> countColumn = new TableColumn<>("数量");
        tableView.getColumns().add(countColumn);
        countColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        countColumn.setCellValueFactory(new PropertyValueFactory<>("count"));
        countColumn.setCellFactory(NumberTableCell.forTableColumn());
        countColumn.setOnEditCommit(event -> {
            short newValue = event.getNewValue().shortValue();
            int row = event.getTablePosition().getRow();
            CashShop.CashItem editItem = tableView.getItems().get(row);
            CashShop.CashItem oriItem = oriList.get(row);
            editItem.setCount(newValue);
            doChange(editItem, oriItem, changeList);
        });

        TableColumn<CashShop.CashItem, Number> priceColumn = new TableColumn<>("价格");
        tableView.getColumns().add(priceColumn);
        priceColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        priceColumn.setCellFactory(NumberTableCell.forTableColumn());
        priceColumn.setOnEditCommit(event -> {
            int newValue = event.getNewValue().intValue();
            int row = event.getTablePosition().getRow();
            CashShop.CashItem editItem = tableView.getItems().get(row);
            CashShop.CashItem oriItem = oriList.get(row);
            editItem.setPrice(newValue);
            doChange(editItem, oriItem, changeList);
        });

        TableColumn<CashShop.CashItem, Number> priorityColumn = new TableColumn<>("优先级");
        tableView.getColumns().add(priorityColumn);
        priorityColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        priorityColumn.setCellValueFactory(new PropertyValueFactory<>("priority"));
        priorityColumn.setCellFactory(NumberTableCell.forTableColumn());
        priorityColumn.setOnEditCommit(event -> {
            int newValue = event.getNewValue().intValue();
            int row = event.getTablePosition().getRow();
            CashShop.CashItem editItem = tableView.getItems().get(row);
            CashShop.CashItem oriItem = oriList.get(row);
            editItem.setPriority(newValue);
            doChange(editItem, oriItem, changeList);
        });

        TableColumn<CashShop.CashItem, Number> periodColumn = new TableColumn<>("有效时间");
        tableView.getColumns().add(periodColumn);
        periodColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        periodColumn.setCellValueFactory(new PropertyValueFactory<>("period"));
        periodColumn.setCellFactory(NumberTableCell.forTableColumn());
        periodColumn.setOnEditCommit(event -> {
            long newValue = event.getNewValue().longValue();
            int row = event.getTablePosition().getRow();
            CashShop.CashItem editItem = tableView.getItems().get(row);
            CashShop.CashItem oriItem = oriList.get(row);
            editItem.setPeriod(newValue);
            doChange(editItem, oriItem, changeList);
        });

        TableColumn<CashShop.CashItem, Number> genderColumn = new TableColumn<>("性别");
        tableView.getColumns().add(genderColumn);
        genderColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.1));
        genderColumn.setCellValueFactory(new PropertyValueFactory<>("gender"));

        TableColumn<CashShop.CashItem, HBox> saleColumn = new TableColumn<>("是否销售");
        tableView.getColumns().add(saleColumn);
        saleColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.2));
        saleColumn.setCellValueFactory(param -> {
            HBox hBox = new HBox();
            hBox.setAlignment(Pos.CENTER);
            SwitchButton switchButton = new SwitchButton();
            switchButton.setOnOff(param.getValue().isOnSale());
            switchButton.onOffProperty().addListener((observable, oldValue, newValue) -> {
                int row = tableView.getSelectionModel().getSelectedIndex();
                CashShop.CashItem editItem = tableView.getItems().get(row);
                CashShop.CashItem oriItem = oriList.get(row);
                editItem.setOnSale(newValue ? 1 : 0);
                doChange(editItem, oriItem, changeList);
            });
            hBox.getChildren().add(switchButton);
            return new SimpleObjectProperty<>(hBox);
        });

        return tableView;
    }

    private void onSearch(Pagination center, TextField searchInput, List<CashShop.CashItem> oriList,
                          List<CashShop.CashItem> changeList) {
        String searchText = searchInput.getText();
        ProgressLoading loading = new ProgressLoading(primaryStage);
        final int PAGE_SIZE = 20;
        loading.add("正在查询总页数", () -> {
            long count = CashItemDao.selectCountByCondition(searchText);
            PlatformImpl.runAndWait(() -> center.setPageCount((int) Math.ceil((double) count / PAGE_SIZE)));
        });
        loading.execute();
        center.setPageFactory(index -> {
            TableView<CashShop.CashItem> tableView = createTableView(oriList, changeList);

            ProgressLoading searchLoading = new ProgressLoading(primaryStage);
            searchLoading.add("正在分页查询商品", () -> {
                // 每次翻页都清空changeList
                changeList.clear();
                ObservableList<CashShop.CashItem> cashItems = FXCollections.observableArrayList();
                oriList.clear();
                oriList.addAll(CashItemDao.selectPageByCondition(index, PAGE_SIZE, searchText));
                oriList.forEach(cashItem -> cashItems.add(cashItem.clone()));
                PlatformImpl.runAndWait(() -> tableView.setItems(cashItems));
            });
            searchLoading.execute();
            return tableView;
        });
    }

    private boolean notChange(CashShop.CashItem editItem, CashShop.CashItem oriItem) {
        return editItem.getCount() == oriItem.getCount() &&
                editItem.getPrice() == oriItem.getPrice() &&
                editItem.getPriority() == oriItem.getPriority() &&
                editItem.getPeriod() == oriItem.getPeriod() &&
                editItem.isOnSale() == oriItem.isOnSale();
    }

    private void doChange(CashShop.CashItem editItem, CashShop.CashItem oriItem, List<CashShop.CashItem> changeList) {
        if (notChange(editItem, oriItem)) {
            changeList.removeIf(change -> NapComUtils.equals(editItem.getSn(), oriItem.getSn()));
            return;
        }
        if (!changeList.contains(editItem)) {
            changeList.add(editItem);
        }
    }
}

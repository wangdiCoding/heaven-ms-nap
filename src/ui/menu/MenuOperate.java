package ui.menu;

import constants.net.ServerConstants;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import net.server.Server;
import tools.Log;
import ui.util.ButtonUtil;
import ui.util.ImageUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;

public class MenuOperate extends MenuAbstract {
    private final String START = "启动";
    private final String STOP = "停止";
    private final String RESTART = "重启";
    private final String REFRESH_STOP = "暂停刷新";
    private final String REFRESH_START = "继续刷新";
    private Timer timer;

    @Override
    public void show() {
        BorderPane root = new BorderPane();
        HBox top = new HBox();
        top.setSpacing(5);
        top.setPadding(new Insets(0, 0, 10, 0));
        Button startButton = ButtonUtil.createGreenButton(START);
        Button stopButton = ButtonUtil.createRedButton(STOP);
        Button restartButton = ButtonUtil.createBlackButton(RESTART);
        top.getChildren().addAll(startButton, stopButton, restartButton);

        TextArea center = new TextArea();
        center.setStyle("-fx-font-size: 14");

        VBox right = new VBox();
        right.setPadding(new Insets(0, 0, 0, 10));
        right.setSpacing(5);

        Tooltip refreshStartTooltip = new Tooltip(REFRESH_START);
        Button refreshStartButton = ImageUtil.createImageButton("refresh_start.png");
        Tooltip.install(refreshStartButton, refreshStartTooltip);


        Tooltip refreshStopTooltip = new Tooltip(REFRESH_STOP);
        Button refreshStopButton = ImageUtil.createImageButton("refresh_stop.png");
        Tooltip.install(refreshStopButton, refreshStopTooltip);

        refreshStartButton.setOnAction(event -> {
            refreshStartButton.setDisable(true);
            refreshStopButton.setDisable(false);
            regRefreshTextArea(center);
        });
        refreshStopButton.setOnAction(event -> {
            refreshStartButton.setDisable(false);
            refreshStopButton.setDisable(true);
            if (timer == null) {
                return;
            }
            timer.cancel();
            timer = null;
        });

        right.getChildren().addAll(refreshStartButton, refreshStopButton);

        refreshStartButton.setDisable(true);
        regRefreshTextArea(center);
        changeButtonStatus(top);
        operate(top, startButton, stopButton, restartButton);
        root.setTop(top);
        root.setCenter(center);
        root.setRight(right);
        setRoot(root);
    }

    @Override
    public void destroy() {
        super.destroy();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void operate(HBox top, Button... buttons) {
        for (Button button : buttons) {
            button.setOnAction(event -> {
                top.getChildren().forEach(child -> child.setDisable(true));
                new Thread(() -> {
                    try {
                        if (START.equals(button.getText())) {
                            Server.getInstance().init();
                        } else {
                            Server.getInstance().shutdownInternal(RESTART.equals(button.getText()));
                        }
                    } catch (Exception e) {
                        Log.error("服务启停发生异常：", e);
                    }

                    changeButtonStatus(top);
                }).start();
            });
        }
    }

    private void changeButtonStatus(HBox top) {
        boolean online = Server.getInstance().isOnline();
        Platform.runLater(() -> top.getChildren().forEach(child -> {
            Button btn = (Button) child;
            if (START.equals(btn.getText())) {
                btn.setDisable(online);
            } else if (STOP.equals(btn.getText())) {
                btn.setDisable(!online);
            } else {
                btn.setDisable(false);
            }
        }));
    }

    private void regRefreshTextArea(TextArea textArea) {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                File logFile = new File(ServerConstants.BASE_DIR + "logs/out.log");
                if (!logFile.exists()) {
                    return;
                }
                if (null == textArea) {
                    return;
                }
                StringBuilder lineBuilder = new StringBuilder();
                try (FileInputStream fis = new FileInputStream(logFile);
                     InputStreamReader bis = new InputStreamReader(fis);
                     BufferedReader reader = new BufferedReader(bis)) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        if (lineBuilder.length() > 524288) {
                            lineBuilder.delete(0, lineBuilder.length() - 524288);
                        }
                        lineBuilder.append(line).append("\n");
                    }
                } catch (Exception e) {
                    Log.error("刷新日志异常：", e);
                }
                Platform.runLater(() -> {
                    double scrollTop = textArea.getScrollTop();
                    textArea.setText(lineBuilder.toString());
                    // 将滚动条滚动到最后一行
                    textArea.positionCaret(textArea.getLength());
                    textArea.selectPositionCaret(textArea.getLength());
                    textArea.setScrollTop(0 == scrollTop ? Double.MAX_VALUE : scrollTop);
                });
            }
        }, 0, 1000);
    }

}


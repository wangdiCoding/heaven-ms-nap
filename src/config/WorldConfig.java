package config;

public class WorldConfig {
    public int worldId = 1;
    public String serverMessage = "Welcome!";
    public String eventMessage = "";
    public String recommendedMessage = "";
    public String channelIds;
    public int expRate = 1;
    public int mesoRate = 1;
    public int dropRate = 1;
    public int bossDropRate = 1;
    public int questRate = 1;
    public int travelRate = 1;
    public int fishingRate = 1;
}
